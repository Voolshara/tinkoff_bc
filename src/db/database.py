import datetime
import os
from contextlib import contextmanager
from typing import Optional

import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import func

engine = sa.create_engine(
    'postgresql://{}:{}@{}:{}/{}'.format(
        os.getenv('TINKOFF_PG_NAME'),
        os.getenv('TINKOFF_PG_PASSWORD'),
        os.getenv('TINKOFF_PG_HOST'),
        os.getenv('TINKOFF_PG_PORT'),
        os.getenv('TINKOFF_PG_DB_NAME'),
    )
)
Session = sessionmaker(bind=engine)
Base = declarative_base()


@contextmanager
def create_session(**kwargs):
    new_session = Session(**kwargs)
    try:
        yield new_session
        new_session.commit()
    except Exception:
        new_session.rollback()
        raise
    finally:
        new_session.close()


class City(Base):
    __tablename__ = 'City'
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String())
    description = sa.Column(sa.String())


class Voice(Base):
    __tablename__ = 'Voice'
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String())


class Users(Base):
    __tablename__ = 'Users'
    id = sa.Column(sa.Integer, primary_key=True)
    tg_user_id = sa.Column(sa.BIGINT, unique=True)
    name_lastname = sa.Column(sa.String())
    city_id = sa.Column(sa.Integer, sa.ForeignKey(City.id))
    trello_board_id = sa.Column(sa.String())
    voice_id = sa.Column(sa.Integer, sa.ForeignKey(Voice.id))
    state = sa.Column(sa.String())


class Chats(Base):
    __tablename__ = 'Chats'
    id = sa.Column(sa.Integer, primary_key=True)
    tg_chat_id = sa.Column(sa.BIGINT, unique=True)
    inviter_id = sa.Column(sa.BIGINT)
    title = sa.Column(sa.String())
    type = sa.Column(sa.String())
    city_id = sa.Column(sa.Integer, sa.ForeignKey(City.id))
    trello_board_id = sa.Column(sa.String())
    voice_id = sa.Column(sa.Integer, sa.ForeignKey(Voice.id))
    status = sa.Column(sa.String())
    state = sa.Column(sa.String())


class Chat_Admin(Base):
    __tablename__ = 'Chat_Admin'
    id = sa.Column(sa.INTEGER, primary_key=True)
    chat_id = sa.Column(sa.INTEGER, sa.ForeignKey(Chats.id))
    user_id = sa.Column(sa.INTEGER)


class History(Base):
    __tablename__ = 'History'
    id = sa.Column(sa.INTEGER, primary_key=True)
    tg_id = sa.Column(sa.INTEGER, sa.ForeignKey(Users.tg_user_id))
    date = sa.Column(sa.DateTime, server_default=func.now())
    message = sa.Column(sa.String())


class Currency(Base):
    __tablename__ = 'Currency'
    id = sa.Column(sa.INTEGER, primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey(Users.id))
    chat_id = sa.Column(sa.Integer, sa.ForeignKey(Chats.id))
    currency = sa.Column(sa.String())


class User_Sub(Base):
    __tablename__ = 'User_Sub'
    id = sa.Column(sa.INTEGER, primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey(Users.tg_user_id))
    chat_id = sa.Column(sa.Integer, sa.ForeignKey(Chats.tg_chat_id))
    name = sa.Column(sa.String())
    time = sa.Column(sa.DateTime)
    utc_offset = sa.Column(sa.INTEGER)
    is_active = sa.Column(sa.BOOLEAN)


class DB_search:
    def __init__(self) -> None:
        pass

    def check_city(self, city: str) -> Optional[int]:
        with create_session() as session:
            city_response = session.query(City).filter(City.name == city).one_or_none()
            return city_response.id if city_response is not None else None

    def check_voice(self, voice: str) -> Optional[int]:
        with create_session() as session:
            voice_response = session.query(Voice).filter(Voice.name == voice).one_or_none()
            return voice_response.id if voice_response is not None else None

    def get_voice(self, tg_id: int) -> Optional[int]:
        with create_session() as session:
            if tg_id > 0:
                user_response = session.query(Users).filter(Users.tg_user_id == tg_id).one_or_none()
                if user_response is None:
                    return None
            else:
                user_response = session.query(Chats).filter(Chats.tg_chat_id == tg_id).one_or_none()
                if user_response is None:
                    return None
            with create_session() as session:
                voice_response = session.query(Voice).filter(Voice.id == user_response.voice_id).one_or_none()
                return voice_response.name if voice_response is not None else None

    def check_user(self, tg_id: int) -> Optional[int]:
        with create_session() as session:
            user_response = session.query(Users).filter(Users.tg_user_id == tg_id).one_or_none()
            return user_response.id if user_response is not None else None

    def check_chat(self, tg_id: int) -> Optional[int]:
        with create_session() as session:
            chat_response = session.query(Chats).filter(Chats.tg_chat_id == tg_id).one_or_none()
            return chat_response.id if chat_response is not None else None

    def get_state(self, tg_id: int) -> str:
        if tg_id < 0:
            with create_session() as session:
                chat_response = session.query(Chats).filter(Chats.tg_chat_id == tg_id).one_or_none()
                return chat_response.state
        with create_session() as session:
            user_response = session.query(Users).filter(Users.tg_user_id == tg_id).one_or_none()
            return user_response.state

    def get_city(self, tg_id: int):
        if tg_id < 0:
            with create_session() as session:
                chat_response = session.query(Chats).filter(Chats.tg_chat_id == tg_id).one_or_none()
                return chat_response.city_id
        with create_session() as session:
            user_response = session.query(Users).filter(Users.tg_user_id == tg_id).one_or_none()
            return user_response.city_id

    def get_city_name(self, city_id):
        with create_session() as session:
            city_response = session.query(City).filter(City.id == city_id).one_or_none()
            return city_response.name

    def get_trello(self, tg_id: int):
        if tg_id < 0:
            with create_session() as session:
                chat_response = session.query(Chats).filter(Chats.tg_chat_id == tg_id).one_or_none()
                return chat_response.trello_board_id
        with create_session() as session:
            user_response = session.query(Users).filter(Users.tg_user_id == tg_id).one_or_none()
            return user_response.trello_board_id

    def get_currency(self, tg_id: int):
        if tg_id > 0:
            with create_session() as session:
                response = session.query(Users).filter(Users.tg_user_id == tg_id).one_or_none()
                curr_response = session.query(Currency).filter(Currency.user_id == response.id).all()
                return [i.currency for i in curr_response]
        with create_session() as session:
            response = session.query(Users).filter(Users.tg_user_id == tg_id).one_or_none()
            curr_response = session.query(Currency).filter(Currency.chat_id == response.id).all()
            return [i.currency for i in curr_response]

    def get_user_sub(self, tg_id):
        if tg_id > 0:
            with create_session() as session:
                response = (
                    session.query(User_Sub)
                    .filter(User_Sub.user_id == tg_id, User_Sub.is_active.is_(True), User_Sub.time != None)
                    .all()
                )
                return [[i.id, i.user_id, i.name, i.time, i.utc_offset] for i in response]
        with create_session() as session:
            response = (
                session.query(User_Sub)
                .filter(User_Sub.chat_id == tg_id, User_Sub.is_active.is_(True), User_Sub.time != None)
                .all()
            )
            return [[i.id, i.chat_id, i.name, i.time, i.utc_offset] for i in response]

    def get_task_data(self, task_id: int):
        with create_session() as session:
            i = session.query(User_Sub).filter(User_Sub.id == task_id).one_or_none()
            try:
                return [i.user_id if i.user_id is not None else i.chat_id, i.name, i.is_active, i.time, i.utc_offset]
            except:
                return None


class DB_new:
    def __init__(self) -> None:
        self.db_search = DB_search()

    def new_city(self, name: str, desc: Optional[str]) -> Optional[int]:
        with create_session() as session:
            session.add(City(name=''.join(name.lower().split()), description=desc))
        return self.db_search.check_city(''.join(name.lower().split()))

    def new_voice(self, name: str) -> None:
        with create_session() as session:
            session.add(Voice(name=name))

    def new_user(
        self, tg_id: int, name: str, city: Optional[str], trello_board_id: Optional[str], voice_id: Optional[int]
    ) -> None:
        with create_session() as session:
            session.add(
                Users(
                    tg_user_id=tg_id,
                    name_lastname=name,
                    city_id=city,
                    trello_board_id=trello_board_id,
                    voice_id=voice_id,
                    state='base',
                )
            )

    def new_chat(
        self,
        tg_id: int,
        inv_id: int,
        title: str,
        chat_type: str,
        city_id: Optional[int] = None,
        trello: Optional[str] = None,
        v_id: Optional[str] = None,
    ):
        with create_session() as session:
            session.add(
                Chats(
                    tg_chat_id=tg_id,
                    inviter_id=inv_id,
                    title=title,
                    type=chat_type,
                    city_id=city_id,
                    trello_board_id=trello,
                    voice_id=v_id,
                    status='active',
                    state='base',
                )
            )

    def set_city(self, tg_id: int, city: str) -> None:
        city_i = self.db_search.check_city(''.join(city.lower().split()))
        if city_i is None:
            city_i = self.new_city(city, None)
        if tg_id > 0:
            with create_session() as session:
                session.query(Users).filter(Users.tg_user_id == tg_id).update({Users.city_id: city_i})
                return
        with create_session() as session:
            session.query(Chats).filter(Chats.tg_chat_id == tg_id).update({Chats.city_id: city_i})

    def set_currency(self, tg_id: int, curr: str):
        if tg_id > 0:
            with create_session() as session:
                response = session.query(Users).filter(Users.tg_user_id == tg_id).one_or_none()
                session.add(Currency(user_id=response.id, chat_id=None, currency=curr))
        else:
            with create_session() as session:
                response = session.query(Chats).filter(Chats.tg_user_id == tg_id).one_or_none()
                session.add(Currency(user_id=None, chat_id=response.id, currency=curr))

    def delete_currency(self, tg_id: int):
        if tg_id > 0:
            with create_session() as session:
                response = session.query(Users).filter(Users.tg_user_id == tg_id).one_or_none()
                session.query(Currency).filter(Currency.user_id == response.id).delete()
            return
        with create_session() as session:
            response = session.query(Chats).filter(Chats.tg_user_id == tg_id).one_or_none()
            session.query(Currency).filter(Currency.chat_id == response.id).delete()

    def set_trello(self, tg_id: int, trello: str) -> None:
        if tg_id > 0:
            with create_session() as session:
                session.query(Users).filter(Users.tg_user_id == tg_id).update({Users.trello_board_id: trello})
            return
        with create_session() as session:
            session.query(Chats).filter(Chats.tg_chat_id == tg_id).update({Chats.trello_board_id: trello})

    def set_voice(self, tg_id: int, voice: str) -> None:
        voice_i = self.db_search.check_voice(voice)
        if tg_id > 0:
            with create_session() as session:
                session.query(Users).filter(Users.tg_user_id == tg_id).update({Users.voice_id: voice_i})
        else:
            with create_session() as session:
                session.query(Chats).filter(Chats.tg_chat_id == tg_id).update({Chats.voice_id: voice_i})

    def set_state(self, tg_id: int, state: str) -> None:
        if tg_id > 0:
            with create_session() as session:
                session.query(Users).filter(Users.tg_user_id == tg_id).update({Users.state: state})
            return
        with create_session() as session:
            session.query(Chats).filter(Chats.tg_chat_id == tg_id).update({Chats.state: state})

    def set_sub(self, tg_id: int, name: str):
        if tg_id > 0:
            with create_session() as session:
                session.add(User_Sub(user_id=tg_id, chat_id=None, name=name, time=None, utc_offset=-3, is_active=True))
                return
        with create_session() as session:
            session.add(User_Sub(user_id=None, chat_id=tg_id, name=name, time=None, utc_offset=-3, is_active=True))

    def set_sub_time(self, tg_id: int, time: str):
        if tg_id > 0:
            with create_session() as session:
                session.query(User_Sub).filter(
                    User_Sub.user_id == tg_id, User_Sub.is_active.is_(True), User_Sub.time == None
                ).update({User_Sub.time: time})
                return
        with create_session() as session:
            session.query(User_Sub).filter(
                User_Sub.chat_id == tg_id, User_Sub.is_active.is_(True), User_Sub.time == None
            ).update({User_Sub.time: time, User_Sub.is_active: True})

    def set_sub_zone(self, tg_id: int, zone: int):
        if tg_id > 0:
            with create_session() as session:
                session.query(User_Sub).filter(User_Sub.user_id == tg_id, User_Sub.is_active.is_(True)).update(
                    {User_Sub.utc_offset: zone}
                )
                return
        with create_session() as session:
            session.query(User_Sub).filter(User_Sub.chat_id == tg_id, User_Sub.is_active.is_(True)).update(
                {User_Sub.utc_offset: zone}
            )
            return

    def delete_sub(self, tg_id: int):
        if tg_id > 0:
            with create_session() as session:
                session.query(User_Sub).filter(User_Sub.user_id == tg_id).update({User_Sub.is_active: False})
                return
        with create_session() as session:
            session.query(User_Sub).filter(User_Sub.chat_id == tg_id).update({User_Sub.is_active: False})

    def new_admin_chat(self, chat_id: int, user_id: int) -> None:
        chat_i = self.db_search.check_Chat(chat_id)
        with create_session() as session:
            session.add(Chat_Admin(chat_id=chat_i, user_id=user_id))

    def new_history_element(self, tg_id: int, message: str):
        with create_session() as session:
            session.add(History(tg_id=tg_id, message=message))

    def create_all_tables(self):
        Base.metadata.create_all(engine)
        self.set_all_voices()

    def set_all_voices(self):
        ALL_VOICES = ['alyona', 'oleg', 'maxim', 'flirt']
        for voice in ALL_VOICES:
            self.new_voice(voice)


# DBS = DB_search()
# print(DBS.check_city(''.join('Чебоксары'.lower().split())))
# DBN = DB_new()
# DBN.set_all_voices()
# DBN.create_all_tables()
# DBN.new_user('12432', 'rrrrr', None, None, None)
# DBN.set_user_city('12432', 'Чебоксары')

# DBN.new_city('Чебоксары', 'Лучший город планеты')
# DBN.new_city('Москва', 'Столица России')
# DBN.new_history_element('234022', 'TEST')
