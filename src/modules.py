import datetime
import os
from typing import Optional

import jinja2
import requests

from src.compliment import randCompliment
from src.memes import meme


ALL_STICKERS = {
    'good': 'CAACAgIAAxkBAAEComNg_dfalIX-B0ZPSFq8BQk71goxrQAC5wwAAk9T4Evq8xVgPr_reCAE',
    'bad': 'CAACAgIAAxkBAAEContg_djUHgtWG-gKGV11jA2Bal6wNwACpwwAAleq4Uv44fPXIphvNSAE',
    'love': 'CAACAgIAAxkBAAEComVg_dfsEdBS-ETZgsHkZAcMoJa9cgACSQ8AAjLz2Evkerw8bFHEqSAE',
    'travel': 'CAACAgIAAxkBAAECo5xg_rdPIyPlLvUfk08Jconi9ElD5wACaA8AAm0v4EvtJ-k8X-LqRCAE',
    'very_funny': 'CAACAgIAAxkBAAEComdg_dgAAW0QGffdO2AKTt9WoRQw1AsAAuUPAAJJ8OBLOaNEidvthS8gBA',
    'so_cool': 'CAACAgIAAxkBAAEComlg_dgvC8S2FQuGhJa65XlP_oNjQwACgA4AAn_P4EtPCbjyuvRxeyAE',
    'slepp': 'CAACAgIAAxkBAAEComtg_dhCbTLaKYRwcJh2kcX5Q3dBxwACWAwAAvQF4UsY7Xz0s8RdBiAE',
    'sad': 'CAACAgIAAxkBAAECom9g_dhdF_6IcB_zbFljabNibeZLoAAC9QwAAmNX4EtmlKuNDfdcuiAE',
    'angry': 'CAACAgIAAxkBAAEConFg_dhoiIPBirJleHoksJXgOe0onQACMg0AAinq4EvaCeLq5QQv-CAE',
    'nice_love': 'CAACAgIAAxkBAAEConNg_diDvV_VsHqNmyknSa0K2SE12wAC4Q4AAlFA2Uu7I-15xn3GGiAE',
    'holiday': 'CAACAgIAAxkBAAEConVg_difnc3eWzk8BuFUZnt64wPTkAACDhAAAh6s2UuMVI6Gl7YFJyAE',
    'fuck': 'CAACAgIAAxkBAAEConlg_di1W5RdBmI8KDBZXkbwNHmiYQACyREAAugf2Uv9L_0_Gc0g4CAE',
    'hi': 'CAACAgIAAxkBAAECon1g_djq2TW8d4s95BYK7OCJYchYyAACTwwAAp8O4EsEYm_CwYNUQCAE',
    'thx': 'CAACAgIAAxkBAAECon9g_dj16OTxyuZcA2x0-DBVnILJuAACIw0AAlWb4EurXQYbDwPD8CAE',
    'respect': 'CAACAgIAAxkBAAECooFg_dj_nGbcQ6bZ4aMxWGSgTfdvbwACFBAAAnKQ4Ety7g78QHdPpSAE',
    'oh': 'CAACAgIAAxkBAAECooNg_dkMcw-QYHjEy-j1O5We8vMsJQACwwsAAjKn2EsHCMMxXu3GZSAE',
    'secret': 'CAACAgIAAxkBAAECoodg_dkh99TyfQHG6ykFH0Ol_QPuKwACXQwAAuSF2UuMf90KAAGLU5ogBA',
    'suprise': 'CAACAgIAAxkBAAECoolg_dktprcFQyYk0Mxkh7v489LdMgAC2woAAmdv4UsqR79ic0yYNyAE',
    'funny': 'CAACAgIAAxkBAAECootg_dkvRC7bMUtYB-vgg_L-L8HNHQACVw0AAvdj4UuTcabyIO9cIyAE',
    'laptop': 'CAACAgIAAxkBAAECoo1g_dlhCv4VU4XzJa231h2KAVgUPwAC4g4AAqW14UtieYwJr5o4rSAE',
    'calmness': 'CAACAgIAAxkBAAECoo9g_dluQh1uOgJfal9l8hTDoI1E0QACThEAAkjL4EsKtVTJVElMTyAE',
    'ban': 'CAACAgIAAxkBAAECopNg_dmci8vK-kKtf2VFSeM5i9tCWAACDREAApKL2Ut8U5r4QBDLZCAE',
    'raise_hand': 'CAACAgIAAxkBAAECopVg_dmwS-DkT34RVncy-1cSKgYNeAACqg4AAuEp2EtpeThUAfVDAyAE',
    'dont_know': 'CAACAgIAAxkBAAECopVg_dmwS-DkT34RVncy-1cSKgYNeAACqg4AAuEp2EtpeThUAfVDAyAE',
    'stupid': 'CAACAgIAAxkBAAECoplg_dn26m8ZSm-dokkZ5j_MoayXsgAChQ0AAhTF4UsbE7pVfrpRpCAE',
    'base': 'CAACAgIAAxkBAAECoqJg_doSuuqYrDEuh2bEScD3New5iQACAxIAAu8o2Usy4LRA-l9JHSAE',
    'news': 'CAACAgIAAxkBAAECo55g_rfcFYKDMw2IKAkAAbw1sqf2CZkAAiMNAAJVm-BLq10GGw8Dw_AgBA',
    'think': 'CAACAgIAAxkBAAECo6Bg_rilY7Hz1UTrc-FGrjScxovYiwAC0g4AAt_02EsWH3G9DrHiEyAE',
    'setting': 'CAACAgIAAxkBAAECo6Jg_rlTj98swsG1ZzMBROSotFJO0wACaRAAAqzQ4Et-5q7ZqwqFESAE',
    'delete': 'CAACAgIAAxkBAAECo6Rg_ryZWZlLtGK2Ub3XpmH5cX-SQgACDREAApKL2Ut8U5r4QBDLZCAE',
    '2_hands': 'CAACAgIAAxkBAAECo6Zg_rz5PajEUD7F1C0270SOI2rSUgACIw0AAlWb4EurXQYbDwPD8CAE',
    'work': 'CAACAgIAAxkBAAECo6hg_r4XnjfR0CqvlK515Qj2qgQjeQAC4g4AAqW14UtieYwJr5o4rSAE',
    'game': 'CAACAgIAAxkBAAECo6pg_r4uEVFAcDVheXH9jgxogcLuvAACDhAAAh6s2UuMVI6Gl7YFJyAE',
}


def get_weather(city: str) -> Optional[dict]:
    try:
        code_to_smile = {
            "Clear": "Ясно \U00002600 ",
            "Clouds": "Облачно \U00002601",
            "Rain": "Дождь \U00002614 ",
            "Drizzle": "Дождь \U00002614 ",
            "Thunderstorm": "Гроза \U000026A1 ",
            "Snow": "Снег \U0001F328 ",
            "Mist": "Туман \U0001F32B",
        }
        open_wheather_token = os.getenv('TINKOFF_OPENWEATHER_TOKEN')
        r = requests.get(
            f"http://api.openweathermap.org/data/2.5/weather?q={city}&appid={open_wheather_token}&units=metric&lang=ru"
        )
        data = r.json()
        city = data["name"]
        cur_wheather = data["main"]["temp"]
        sunrise = datetime.datetime.fromtimestamp(data["sys"]["sunrise"])
        sunrise_time = sunrise.strftime("%H:%M")
        sunset = datetime.datetime.fromtimestamp(data["sys"]["sunset"])
        sunset_time = sunset.strftime("%H:%M")
        wheather_description = data["weather"][0]["main"]
        if wheather_description in code_to_smile:
            wd = code_to_smile[wheather_description]
        else:
            wd = "происходит что-то непонятное - посмтори в окно или попробуй ещё раз"
        templateLoader = jinja2.FileSystemLoader(searchpath="./")
        templateEnv = jinja2.Environment(loader=templateLoader)
        JINJA_FILE = "/src/jinja_templates/weather.jinja"
        template = templateEnv.get_template(JINJA_FILE)
        templateVars = dict(
            city=city, cur_wheather=cur_wheather, wd=wd, sunrise_time=sunrise_time, sunset_time=sunset_time
        )
        return dict(message=[template.render(templateVars)])
    except:
        return None


def memes() -> dict:
    """Send Memes"""
    return dict(photo=[meme()])


def curr_prepare(currency_value: float) -> float:
    return '%.2f' % float(1 / currency_value)


def currencies(curr_names_arr: list) -> dict:
    """Send Currencies"""
    res = requests.get('https://www.cbr-xml-daily.ru/latest.js')
    if res.status_code != 200:
        return None
    curr = res.json()['rates']
    for i in range(len(curr_names_arr)):
        name = curr_names_arr[i]
        if name == 'GBP':
            smile = '💷'
        elif name == 'EUR':
            smile = '💶'
        elif name == 'CNY':
            smile = '💴'
        else:
            smile = '💵'
        curr_names_arr[i] = [smile, curr_names_arr[i], curr_prepare(curr[curr_names_arr[i]])]
    templateLoader = jinja2.FileSystemLoader(searchpath="./")
    templateEnv = jinja2.Environment(loader=templateLoader)
    CURR_FILE = "/src/jinja_templates/currencies.jinja"
    template = templateEnv.get_template(CURR_FILE)
    templateVars = {"curr_arr": curr_names_arr}
    return dict(markdown=[template.render(templateVars)])


def compliment_generator() -> dict:
    """Send Compliment"""
    mes = randCompliment()
    return dict(message=[mes])
