from telegram import InlineKeyboardButton, InlineKeyboardMarkup


def currency() -> None:
    keyboard = [
        [
            InlineKeyboardButton("Доллар - 💵 ", callback_data="Dollar"),
            InlineKeyboardButton("Евро - 💶 ", callback_data="Euro"),
        ],
        [
            InlineKeyboardButton("Йен - 💴 ", callback_data="Yen"),
            InlineKeyboardButton("Фунты - 💷", callback_data="Pounds"),
        ],
        [InlineKeyboardButton("Готово", callback_data="Curr_ready"),],
    ]
    return InlineKeyboardMarkup(keyboard)


def plugins() -> None:
    keyboard = [
        [
            InlineKeyboardButton("✅Продуктивность✅", callback_data="works"),
            InlineKeyboardButton("Развлечения", callback_data="games"),
        ],
        [InlineKeyboardButton("⚙️Настройки⚙️", callback_data="Settings")],
    ]
    return InlineKeyboardMarkup(keyboard)


def work() -> None:
    keyboard = [
        [
            InlineKeyboardButton("🧳Travel🧳", callback_data="Travel"),
            InlineKeyboardButton("☀️Погода🌧", callback_data="Wheather"),
        ],
        [
            InlineKeyboardButton("💶Курс Валют💷", callback_data="Currency"),
            InlineKeyboardButton("📝Список дел🗓", callback_data="TO DO"),
        ],
        [
            InlineKeyboardButton("📰Новости📰", callback_data="News_Plugins"),
        ]
    ]
    return InlineKeyboardMarkup(keyboard)


def game() -> None:
    keyboard = [
        [
            InlineKeyboardButton("📊Quiz💬", callback_data="Quiz"),
            InlineKeyboardButton("🎶Music quiz🎶", callback_data="Music_quiz"),
        ],
        [
            InlineKeyboardButton("🐸Мемы🐸", callback_data="Memes"),
            InlineKeyboardButton("Бросок монеты", callback_data="Coin_Flip"),
        ],
        [
            InlineKeyboardButton("Комплимент", callback_data="Compliments"),
        ]
    ]
    return InlineKeyboardMarkup(keyboard)


def news() -> None:
    keyboard = [
    [
        InlineKeyboardButton("Общие", callback_data="news_general")
    ],
    [
        InlineKeyboardButton("Технологии", callback_data="news_tech"),
        InlineKeyboardButton("Бизнес", callback_data="news_business"),
        InlineKeyboardButton("Развлечения", callback_data="news_entertainment")
    ],
    [
        InlineKeyboardButton("Спорт", callback_data="news_sport"),
        InlineKeyboardButton("Здоровье", callback_data="news_health"),
        InlineKeyboardButton("Наука", callback_data="news_science")
    ]
    ]
    return InlineKeyboardMarkup(keyboard)


def settings() -> None:
    keyboard = [
        [
            InlineKeyboardButton("Trello", callback_data="Settings_trello"),
            InlineKeyboardButton("Город", callback_data="Settings_city"),
            InlineKeyboardButton("Валюта", callback_data="Settings_curr"),
            InlineKeyboardButton("Голос", callback_data="Settings_voice"),
        ],
        [InlineKeyboardButton("Подписки", callback_data="Subscriptions"),],
    ]
    return InlineKeyboardMarkup(keyboard)


def voices() -> None:
    keyboard = [
        [
            InlineKeyboardButton("Алёна", callback_data="Alyona_voice_setup"),
            InlineKeyboardButton("Максим", callback_data="Maxim_voice_setup"),
            InlineKeyboardButton("Флирт", callback_data="Flirting_alyona_voice_setup"),
        ],
    ]
    return InlineKeyboardMarkup(keyboard)


def quiz_keyboard() -> None:
    keyboard = [
        [
            InlineKeyboardButton("Экономика", callback_data='category_econom'),
            InlineKeyboardButton("Информатика", callback_data='category_it'),
        ],
        [
            InlineKeyboardButton("Английский язык", callback_data='category_eng'),
            InlineKeyboardButton("Экология", callback_data='category_ecology'),
        ],
        [
            InlineKeyboardButton("Автотематика", callback_data='category_avto'),
            InlineKeyboardButton("Спорт", callback_data='category_sport'),
        ],
        [
            InlineKeyboardButton("Математика", callback_data='category_matan'),
            InlineKeyboardButton("Технологии", callback_data='category_tehno'),
        ],
    ]
    return InlineKeyboardMarkup(keyboard)


def subscriptions_setup():
    keyboard = [
        [
            InlineKeyboardButton("Погода", callback_data='sub_weather'),
            InlineKeyboardButton("Валюта", callback_data='sub_curr'),
        ],
        [
            InlineKeyboardButton("Новости", callback_data='sub_news'),
            InlineKeyboardButton("Trello", callback_data='sub_trello'),
        ],
        [
            InlineKeyboardButton("Готово", callback_data='Subscription_ready'),
            InlineKeyboardButton("🗑 Удалить подписки 🗑", callback_data='Subscription_delete'),
        ],
    ]
    return InlineKeyboardMarkup(keyboard)


def time_zone_buttons():
    keyboard = [
        [InlineKeyboardButton("МСК+0", callback_data='msk+0'),],
        [
            InlineKeyboardButton("МСК-1", callback_data='msk-1'),
            InlineKeyboardButton("МСК+1", callback_data='msk+1'),
            InlineKeyboardButton("МСК+2", callback_data='msk+2'),
            InlineKeyboardButton("МСК+3", callback_data='msk+3'),
            InlineKeyboardButton("МСК+4", callback_data='msk+4'),
        ],
        [
            InlineKeyboardButton("МСК+5", callback_data='msk+5'),
            InlineKeyboardButton("МСК+6", callback_data='msk+6'),
            InlineKeyboardButton("МСК+7", callback_data='msk+7'),
            InlineKeyboardButton("МСК+8", callback_data='msk+8'),
            InlineKeyboardButton("МСК+9", callback_data='msk+9'),
        ],
    ]
    return InlineKeyboardMarkup(keyboard)
