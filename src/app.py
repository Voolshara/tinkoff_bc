from telegram import Update
from telegram.ext import CallbackQueryHandler, ChatMemberHandler, CommandHandler, Filters, MessageHandler, Updater
from typer import Typer

from src.handlers import (
    base,
    coin_flip,
    help_command,
    member_handler,
    start,
    voice_handler,
    stikers_handler,
    travel,
    weather,
    currency,
    trello,
    quiz,
    music_quiz,
    news_hand,
    memes_hand,
    command_handler,
    settings_hand,
    sub_setup)
from src.inline_handlers import buttons_headlers
from src.quiz import IT, daily, eco, economic, eng, fem

mybot = Typer()


@mybot.command()
def run(*, token: str) -> None:
    updater = Updater(token)

    dispatcher = updater.dispatcher  # type: ignore
    dispatcher.add_handler(ChatMemberHandler(member_handler, chat_member_types=1))

    updater.dispatcher.add_handler(CallbackQueryHandler(buttons_headlers))
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help_command))

    dispatcher.add_handler(CommandHandler('fem', fem))
    
    dispatcher.add_handler(CommandHandler('travel', travel))
    dispatcher.add_handler(CommandHandler('weather', weather))
    dispatcher.add_handler(CommandHandler('currency', currency))
    dispatcher.add_handler(CommandHandler('trello', trello))
    dispatcher.add_handler(CommandHandler('news', news_hand))

    dispatcher.add_handler(CommandHandler('quiz', quiz))
    dispatcher.add_handler(CommandHandler('memes', memes_hand))
    dispatcher.add_handler(CommandHandler('music_quiz', music_quiz))
    dispatcher.add_handler(CommandHandler('coin', coin_flip))
    dispatcher.add_handler(CommandHandler('compliment', command_handler))

    dispatcher.add_handler(CommandHandler('settings', settings_hand))
    dispatcher.add_handler(CommandHandler('sub', sub_setup))

    dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, base))
    dispatcher.add_handler(MessageHandler(Filters.voice, voice_handler))
    dispatcher.add_handler(MessageHandler(Filters.sticker, stikers_handler))

    updater.start_polling()
    updater.start_polling(allowed_updates=Update.ALL_TYPES)
    updater.idle()
