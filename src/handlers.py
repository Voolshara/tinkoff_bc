import datetime
import os
import re
import subprocess
from datetime import timedelta
from random import choice, randint
from typing import Final, cast

from telegram import ChatAction, ForceReply, ParseMode, Poll, Update, User
from telegram.ext import CallbackContext
from tinkoff_voicekit_client import ClientSTT, ClientTTS

from src.db.database import DB_new, DB_search, Voice
from src.inline_buttons import plugins
from src.state_machine import STATE, state_handler
from src.modules import ALL_STICKERS
from src.inline_buttons import news, subscriptions_setup


def random_name():
    return ''.join([chr(randint(97, 122)) for i in range(8)])


token: Final[str] = "1736559730:AAHVW1DjYqMxWdKNanUCPHOcAZYcuKQsMvM"
DBN = DB_new()
DBS = DB_search()

HELLO_MATCH = re.compile(r"([Пп][Рр][иеИЕ][Вв][иеИЕ][Тт])|([Hh][Ee][Ll]+[Oo])")

open_wheather_token = 'bfa8d426b626bbfadae0dfb17496323c'

COMMAND_HANDLERS = {
    'travel': 'спланируй поездку',
    'weather': 'хочу погоду',
    'currency': 'валюты',
    'trello': 'выведи задания',
    'quiz': 'хочу квиз',
    'music-quiz': 'начни музыкальную викторину',
    'memes': 'мемы',
    'compliments': 'комплименты',
} 


class TTS:
    def __enter__(self):
        return self.AUDIO_OBJ

    def __exit__(self, *args, **kwargs):
        os.remove(f'./src/voice_kid/generate/{self.file_name}_0.wav')

    def __init__(self, message, voice):
        self.file_name = random_name()
        self.API_KEY = os.getenv('TINKOFF_API_KEY')
        self.SECRET_KEY = os.getenv('TINKOFF_SECRET_KEY')
        self.allowed_characters = {
            'а',
            'б',
            'в',
            'г',
            'д',
            'е',
            'ё',
            'ж',
            'з',
            'и',
            'й',
            'к',
            'л',
            'м',
            'н',
            'о',
            'п',
            'р',
            'с',
            'т',
            'у',
            'ф',
            'х',
            'ц',
            'ч',
            'ш',
            'щ',
            'ь',
            'ы',
            'ъ',
            'э',
            'ю',
            'я',
            '.',
            ',',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '0',
            "a",
            "b",
            "c",
            "d",
            "e",
            "f",
            "g",
            "h",
            "i",
            "j",
            "k",
            "l",
            "m",
            "n",
            "o",
            "p",
            "q",
            "r",
            "s",
            "t",
            "u",
            "v",
            "w",
            "x",
            "y",
            "z",
        }
        out_message = ""
        for letter in message.lower():
            if letter in self.allowed_characters:
                out_message += letter
            elif letter == '\n':
                out_message += '.\n'

        client = ClientTTS(self.API_KEY, self.SECRET_KEY)
        audio_config = {"audio_encoding": "LINEAR16", "sample_rate_hertz": 48000, "voice": {"name": voice}}

        # rows_responses = client.streaming_synthesize("Спокойной ночи мой господин", audio_config)
        client.synthesize_to_audio_wav(out_message, audio_config, "./src/voice_kid/generate/{}".format(self.file_name))
        self.AUDIO_OBJ = None
        with open(f'./src/voice_kid/generate/{self.file_name}_0.wav', 'rb') as audio_file:
            self.AUDIO_OBJ = audio_file.read()


class STT:
    def __enter__(self):
        return self.out

    def __exit__(self, *args, **kwargs):
        os.remove(self.direcory.format(self.file_name + '.wav'))
        os.remove(self.direcory.format(self.file_name + '.oga'))

    def __init__(self, file_name):
        self.file_name = file_name
        self.API_KEY = os.getenv('TINKOFF_API_KEY')
        self.SECRET_KEY = os.getenv('TINKOFF_SECRET_KEY')

        self.direcory = './src/voice_kid/recognize/{}'
        process = subprocess.run(
            ['ffmpeg', '-i', self.direcory.format(file_name + '.oga'), self.direcory.format(file_name + '.wav')]
        )
        if process.returncode != 0:
            raise Exception("Something went wrong")
        client = ClientSTT(self.API_KEY, self.SECRET_KEY)
        audio_config = {"encoding": "LINEAR16", "sample_rate_hertz": 48000, "num_channels": 1}
        response = client.recognize(self.direcory.format(file_name + '.wav'), audio_config)
        self.out = ' '.join([i['alternatives'][0]['transcript'] for i in response['results']])


def voice_prepare(s, id):
    voice = DBS.get_voice(id)
    if voice is None:
        return None
    with TTS(s, voice) as f:
        return f


def send_out(update: Update, out_data: dict, need_voice: bool) -> None:
    if need_voice == False:
        need_voice = True
    if update.message is not None:
        for data_type in out_data:
            if data_type == 'stickers':
                for el in out_data[data_type]:
                    update.message.reply_sticker(ALL_STICKERS[el])
            elif data_type == 'message':
                for el in out_data[data_type]:
                    update.message.reply_text(el, reply_markup=out_data['buttons'] if 'buttons' in out_data else None)
                    if need_voice:
                        voice = voice_prepare(el, update.message.chat_id)
                        if voice is not None:
                            update.message.reply_voice(voice)
            elif data_type == 'markdown':
                for el in out_data[data_type]:
                    update.message.reply_markdown_v2(
                        el, reply_markup=out_data['buttons'] if 'buttons' in out_data else None
                    )
                    if need_voice:
                        voice = voice_prepare(el, update.message.chat_id)
                        if voice is not None:
                            update.message.reply_voice(voice)
            elif data_type == 'photo':
                for el in out_data[data_type]:
                    update.message.reply_photo(el, reply_markup=out_data['buttons'] if 'buttons' in out_data else None)
            elif data_type == 'poll':
                for el in out_data[data_type]:
                    update.message.reply_poll(el[0], el[1], type=Poll.QUIZ, correct_option_id=el[2])
            elif data_type == 'music':
                for el in out_data[data_type]:
                    with open(el, 'rb') as file:
                        update.message.reply_voice(file.read())
                    # update.message.reply_poll(el[0], el[1], type=Poll.QUIZ, correct_option_id=el[2])

    elif update.effective_chat is not None:
        for data_type in out_data:
            if data_type == 'stickers':
                for el in out_data[data_type]:
                    update.effective_chat.send_sticker(ALL_STICKERS[el])
            elif data_type == 'message':
                for el in out_data[data_type]:
                    update.effective_chat.send_message(
                        el, reply_markup=out_data['buttons'] if 'buttons' in out_data else None)
                    if need_voice:
                        voice = voice_prepare(el, update.effective_chat.id)
                        if voice is not None:
                            update.effective_chat.send_voice(voice)
            elif data_type == 'markdown':
                for el in out_data[data_type]:
                    update.effective_chat.send_message(
                        el,
                        reply_markup=out_data['buttons'] if 'buttons' in out_data else None,
                        parse_mode=ParseMode.MARKDOWN_V2,
                    )
                    if need_voice:
                        voice = voice_prepare(el, update.effective_chat.id)
                        if voice is not None:
                            update.effective_chat.send_voice(voice)
            elif data_type == 'photo':
                for el in out_data[data_type]:
                    update.effective_chat.send_photo(
                        el, reply_markup=out_data['buttons'] if 'buttons' in out_data else None
                    )
            elif data_type == 'poll':
                for el in out_data[data_type]:
                    update.effective_chat.send_poll(el[0], el[1], type=Poll.QUIZ, correct_option_id=el[2])
            elif data_type == 'music':
                for el in out_data[data_type]:
                    with open(el, 'rb') as file:
                        update.effective_chat.send_voice(file.read())

    elif update.effective_user is not None:
        for data_type in out_data:
            if data_type == 'stickers':
                for el in out_data[data_type]:
                    update.effective_user.send_sticker(ALL_STICKERS[el])
            elif data_type == 'message':
                for el in out_data[data_type]:
                    update.effective_user.send_message(
                        el, reply_markup=out_data['buttons'] if 'buttons' in out_data else None
                    )
                    if need_voice:
                        voice = voice_prepare(el, update.effective_user.id)
                        if voice is not None:
                            update.effective_user.send_voice(voice)
            elif data_type == 'markdown':
                for el in out_data[data_type]:
                    update.effective_user.send_message(
                        el,
                        reply_markup=out_data['buttons'] if 'buttons' in out_data else None,
                        parse_mode=ParseMode.MARKDOWN_V2,
                    )
                    if need_voice:
                        voice = voice_prepare(el, update.effective_user.chat_id)
                        if voice is not None:
                            update.effective_user.send_voice(voice)
            elif data_type == 'photo':
                for el in out_data[data_type]:
                    update.effective_user.send_photo(
                        el, reply_markup=out_data['buttons'] if 'buttons' in out_data else None
                    )
            elif data_type == 'poll':
                for el in out_data[data_type]:
                    update.effective_user.send_poll(el[0], el[1], type=Poll.QUIZ, correct_option_id=el[2])
            elif data_type == 'music':
                for el in out_data[data_type]:
                    with open(el, 'rb') as file:
                        update.effective_user.send_voice(file.read())


def start(update: Update, context: CallbackContext) -> None:  # pylint: disable=W0613
    """Send a message when the command /start is issued."""
    user = cast(User, update.effective_user)
    if DBS.check_user(update.effective_user.id) is None:
        DBN.new_user(update.effective_user.id, update.effective_user.full_name, None, None, None)
    update.message.reply_sticker(ALL_STICKERS['hi'])
    update.message.reply_markdown_v2(
        fr'Hi {user.mention_markdown_v2()}\!', reply_markup=ForceReply(selective=True),
    )
    update.message.reply_text(
        f"""Привет, я Элли - Ваш персональный бот помощник. Я помогу вам оставаться в курсе событий. Вот, что я умею : """,
        reply_markup=plugins(),
    )


def help_command(update: Update, context: CallbackContext) -> None:  # pylint: disable=W0613
    """Send a message when the command /help is issued."""
    update.message.reply_text("""Продуктивность:
/travel - подскажу, какие места посетить во время путешествия по России
/weather - сообщу погоду в любой точке земного шара
/currency - расскажу о курсе интересующей вас валюты
/trello - напомню о работе на день каждому члену беседы
/news - новости, свежие, как Ленин в мавзолее""")
    update.message.reply_text("""Развлечения:
/quiz - проведу для тебя квиз на выбранную тему
/memes - покажу самый актуальный мем
/music_quiz - проведу для тебя музыкальную викторину 
/coin - решу важные вопросы
/compliment - расскажу о тебе правду""")
    update.message.reply_text("""Настрока бота
/settings - настрой меня 
/sub - настрой подписки
    """)


def base(update: Update, context: CallbackContext) -> None:  # pylint: disable=W0613
    """Echo the user message."""
    if update.effective_chat is None:
        context.bot.send_chat_action(chat_id=update.effective_user.id, action=ChatAction.TYPING, timeout=1)
        DBN.new_history_element(update.effective_user.id, update.message.text)
        now_state = DBS.get_state(update.effective_user.id)

        out_data = state_handler(STATE(update.effective_user.id, update.message.text, now_state))
        if 'is_recognize' in out_data:
            is_recognize = out_data['is_recognize']
        else:
            is_recognize = False
        send_out(update, out_data, is_recognize)
    else:
        context.bot.send_chat_action(chat_id=update.effective_chat.id, action=ChatAction.TYPING, timeout=1)
        now_state = DBS.get_state(update.effective_chat.id)
        out_data = state_handler(STATE(update.effective_chat.id, update.message.text, now_state))
        if 'is_recognize' in out_data:
            is_recognize = out_data['is_recognize']
        else:
            is_recognize = False
        send_out(update, out_data, is_recognize)


def voice_handler(update: Update, context: CallbackContext) -> None:
    bot = context.bot
    f = bot.getFile(update.message.voice.file_id)
    file_name = random_name()
    f.download('./src/voice_kid/recognize/{}.oga'.format(file_name))
    with STT(file_name) as t:
        context.bot.send_chat_action(chat_id=update.effective_user.id, action=ChatAction.TYPING, timeout=1)
        DBN.new_history_element(update.effective_user.id, t)
        now_state = DBS.get_state(update.effective_user.id)

        out_data = state_handler(STATE(update.effective_user.id, t.lower(), now_state))
        send_out(update, out_data, True)


def coin_flip(update: Update, context: CallbackContext) -> None:
    coin_states = ['Решка', 'Орёл']
    send_out(update, dict(message=[choice(coin_states)]), True)

def settings_hand(update: Update, context: CallbackContext) -> None:
    command_handler(update, 'настройки')

def sub_setup(update: Update, context: CallbackContext) -> None:
    update.message.reply_text('Какие уведомления вы хотите получать?', reply_markup=subscriptions_setup())

def news_hand(update: Update, context: CallbackContext) -> None:
    update.message.reply_sticker(ALL_STICKERS['news'])
    update.message.reply_text("Выберите тему новости", reply_markup=news())

def memes_hand(update: Update, context: CallbackContext) -> None:
    command_handler(update, 'memes')

def compliment_hand(update: Update, context: CallbackContext) -> None:
    command_handler(update, 'compliments')

def travel(update: Update, context: CallbackContext) -> None:
    command_handler(update, 'travel')


def weather(update: Update, context: CallbackContext) -> None:
    command_handler(update, 'weather')


def currency(update: Update, context: CallbackContext) -> None:
    command_handler(update, 'currency')


def trello(update: Update, context: CallbackContext) -> None:
    command_handler(update, 'trello')


def quiz(update: Update, context: CallbackContext) -> None:
    command_handler(update, 'quiz')


def music_quiz(update: Update, context: CallbackContext) -> None:
    command_handler(update, 'music-quiz')


def command_handler(update: Update, what: str) -> None:
    if update.effective_chat is None:
        now_state = DBS.get_state(update.effective_user.id)
        out_data = state_handler(STATE(update.effective_user.id, COMMAND_HANDLERS[what], now_state))
        if 'is_recognize' in out_data:
            is_recognize = out_data['is_recognize']
        else:
            is_recognize = False
        send_out(update, out_data, is_recognize)
    else:
        now_state = DBS.get_state(update.effective_chat.id)
        out_data = state_handler(STATE(update.effective_chat.id, COMMAND_HANDLERS[what], now_state))
        if 'is_recognize' in out_data:
            is_recognize = out_data['is_recognize']
        else:
            is_recognize = False
        send_out(update, out_data, is_recognize)


def member_handler(update: Update, context: CallbackContext) -> None:
    if update.my_chat_member is not None:
        if update.my_chat_member.new_chat_member.status == 'member':
            new_chat_id = update.my_chat_member.chat.id
            new_chat_inviter_id = update.my_chat_member.from_user.id
            new_chat_title = update.my_chat_member.chat.title
            new_chat_type = update.my_chat_member.chat.type
            DBN.new_chat(new_chat_id, new_chat_inviter_id, new_chat_title, new_chat_type)
        elif update.my_chat_member.new_chat_member.status == 'left':
            DBN.set_chat_status(update.my_chat_member.chat.id, 'deleted')
    else:
        if update.chat_member.new_chat_member.status == 'member':
            new_member_name = update.chat_member.new_chat_member.user.username
            mention_markdown = update.chat_member.new_chat_member.user.mention_markdown_v2()
            update.effective_chat.send_message(
                random_greeting(new_member_name, mention_markdown), parse_mode=ParseMode.MARKDOWN_V2
            )
        for i in update.effective_chat.get_administrators():
            DBN.new_admin_chat(update.effective_chat.id, i.user.id)


def count_delay(delay: str) -> int:
    rigth_now = datetime.datetime.utcnow()
    needed_time = datetime.datetime.strptime(delay, "%H:%M")
    dt_obj = datetime.datetime.now().replace(
        hour=needed_time.hour, minute=needed_time.minute, second=00, microsecond=00
    )
    if dt_obj.hour < rigth_now.hour:
        dt_obj = dt_obj + timedelta(days=1)
    elif dt_obj.hour == rigth_now.hour and dt_obj.minute < rigth_now.minute:
        dt_obj = dt_obj + timedelta(days=1)
    our_delay = (dt_obj.timestamp() * 1000) - (rigth_now.timestamp() * 1000)
    return our_delay


def random_greeting(user_name: str, user_link) -> str:
    Greetings = [
        f"Мы рады видеть вас на борту, {user_link}!",
        f"Мы очень рады видеть вас, {user_link}!",
        f"Добро пожаловать, {user_link} - давайте узнаем друг-друга получше!",
        f"Хорошее решение {user_link} - добро пожаловать в наш чат!",
        f"Приветсвую, {user_link} ! Пути назад уже нет😈",
        f"Какая встреча ! Добро пожаловать, {user_link}.",
        f"А вот и {user_link} - мы рады видеть вас",
        f"Звезды сошлись не по своей траектории, иначе как бы так случилось, что {user_link} зашёл в чат!",
        f"Без тебя этот день не был бы так хорош {user_link} - добро пожаловать",
        f"А вот и вишенка на торте - Рады видеть тебя, {user_link}!",
        f"Пророчесвто гласит, что если {user_link} зашёл в чат, беседа будет оживлённой."
        f"Посмотрите, все гоблины собрались вместе, а всё потому что {user_link} просто золото",
    ]
    return choice(Greetings).replace("!", "\!").replace(".", "\.").replace('-', "\-")


def stikers_handler(update: Update, context: CallbackContext):
    if update.message.sticker.set_name == 'Ellie_bot':
        emoji = update.message.sticker.emoji
        message = 'О, это же я'
        if emoji == '😍':
            message = 'Бесконечно можно смотреть на огонь, закат и лицо Элли'    
        elif emoji == '😂':
            message = 'Хпхпхпхпхп ты такой смешной'
        elif emoji == '😴':
            message =  'Спокойной ночи милашка'
        elif emoji == '😢':
            message = 'За окном rain на душе pain в школу идти again'
        elif emoji == '🥳':
            message = 'Нет ничего лучше чем праздник с Элли'
        elif emoji == '😘':
            message = '*ЧМОК*'
        elif emoji == '🤬':
            message = 'Иногда нужно дать волю эмоциям'
        elif emoji == '👍':
            message = 'Очень хорошо'
        elif emoji == '👎':
            message = 'Можно было лучше'
        elif emoji == '🤗':
            message = 'ПРИВЕЕЕЕЕЕЕЕТ'
        elif emoji == '👊':
            message = 'Респект'
        elif emoji == '🤔':
            message = 'Думать тоже иногда надо' 
        elif emoji == '😱':
            message = 'Ух ты, так круто' 
        elif emoji == '👩‍💻':
            message = 'Работа не волк волк это ходить' 
        elif emoji == '❌':
            message = 'Нет нельзя ни в коем случае!!!'    
        send_out(update, dict(message = [message]), True)