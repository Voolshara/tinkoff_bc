COMPLIMENTS = (
    'Только ты умеешь, так заразительно, весело и искренне смеяться!',
    'Ты настоящий альтруи0ст, - готов дарить людям своё0 хоро0шее настроение круглосуточно!',
    'Ты отличный организатор! Тебе по плечу организовать мероприятие любого уровня!',
    'С тобой приятно проводить время! Я словно возвращаюсь в беззаботное детство',
    'У тебя всегда отлично выглаженная рубашка!',
    'Самый жизнерадостный человек на свете, - это ты!',
    'Ты тот, в ком можно быть уверенным на все сто!',
    'С тобой даже в худшие дни, всегда можно найти минуту радости!',
    'Замечательная у тебя привычка, улыбаться!, Иногда она обезоруживает!',
    'С тобой весело!, Людей, которые с одинаковой радостью пускают мыльные пузыри в любом возрасте, не так уж и много!',
    'Все вокруг такие серьезные, а с тобой можно и подурачиться!',
    'В этом мире так трудно встретить честного и ответственного человека, как ты!',
    'Ты отличаешься от других, странным и упорным оптимизмом!',
    'Поража0юсь твоему позитиву:, для тебя всегда самый лучший день, - это сегодня!',
    'У тебя завидное упрямство!, Даже 99 неудач, тебя не останавливают, ты точно уверен, что все у тебя получится!',
    'Большинство населения планеты, тебе бы позавидовало, - для тебя не существует условностей!',
    'Ты прирожденный руководитель, и лидер!, Тебе бы корпорацию возглавлять, уж ты бы развернулся!',
    'Внешне ты очень серьезный человек, но такого чувства юмора как у тебя, я давно не встречала!',
    'У тебя такой шикарный ремень из крокодиловой кожи! Где ты только находишь такие вещи?',
    'Так бывает очень редко, но у тебя есть способности ко всему, за что ты берё0шься!, Ты удивительный человек!',
    'Ты как Карлсон, - мужчина в полном расцвете сил!, Пусть ты и не летаешь, но у тебя есть огромное преимущество, - ты излучаешь приятное, теплое обаяние!',
)

import random


def randCompliment():
    return COMPLIMENTS[random.randint(0, len(COMPLIMENTS) - 1)]
