from typing import List

import jinja2
import requests
from pydantic import BaseModel


class Cards(BaseModel):
    name: str
    desc: str
    idList: str
    idMembers: List


class Board_Lists(BaseModel):
    name: str


class Card_Member(BaseModel):
    fullName: str


def trello_decorator(func):
    def wrapper(id):
        trello_data = func(id)
        if trello_data is None:
            return None
        else:
            message = 'Список задач на сегодня'
        templateLoader = jinja2.FileSystemLoader(searchpath="./")
        templateEnv = jinja2.Environment(loader=templateLoader)

        FILE = "/src/jinja_templates/trello.jinja"
        template = templateEnv.get_template(FILE)
        templateVars = {"message": message, "ALL_TASKS": trello_data}
        message = template.render(templateVars)
        return [message]

    return wrapper


@trello_decorator
def get_trello_data(id):
    OUT_DICT = dict()
    res = requests.get("https://trello.com/1/boards/{id}/cards/{filter}".format(id=id, filter="visible"))
    if res.status_code != 200:
        return None
    try:
        all_tsk = [Cards.parse_obj(task) for task in res.json()]
        for task in all_tsk:
            res = requests.get(f"https://trello.com/1/lists/{task.idList}")
            if res.status_code != 200:
                continue
            list_name = Board_Lists.parse_obj(res.json()).name
            who = ''
            if len(task.idMembers) != 0:
                res = requests.get(f"https://trello.com/1/members/{task.idMembers[0]}")
                if res.status_code == 200:
                    who = Card_Member.parse_obj(res.json()).fullName
            if list_name in OUT_DICT:
                OUT_DICT[list_name] += task_create(task.name, who)
            else:
                OUT_DICT[list_name] = task_create(task.name, who)
        message = ''
        for task in OUT_DICT:
            templateLoader = jinja2.FileSystemLoader(searchpath="./")
            templateEnv = jinja2.Environment(loader=templateLoader)

            FILE = "/src/jinja_templates/trello_card.jinja"
            template = templateEnv.get_template(FILE)
            templateVars = {"task": task, "task_data": OUT_DICT[task]}
            message += template.render(templateVars)
        return message
    except:
        return None


def task_create(task, people: str) -> str:
    templateLoader = jinja2.FileSystemLoader(searchpath="./")
    templateEnv = jinja2.Environment(loader=templateLoader)

    TASK_FILE = "/src/jinja_templates/task.jinja"
    MEMBER_FILE = "/src/jinja_templates/member.jinja"

    template = templateEnv.get_template(TASK_FILE)
    templateVars = {"data": task}
    outputText = template.render(templateVars)

    if people != '':
        template = templateEnv.get_template(MEMBER_FILE)
        templateVars = {"people": people}
        outputText += template.render(templateVars) + '\n'
    else:
        outputText += '\n'

    return outputText
