from telegram import Poll


def daily(update, context):
    """Отправка заранее определенную викторину"""
    questions = 'Какая деталь внешности отсутствует у женщины, которую изобразил Леонардо да Винчи на своем полотне «Мона Лиза»?'
    answer = ['губы', 'брови', 'нос', 'улыбка']
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=1)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)


def economic(update, context):
    """Отправка заранее определенную викторину"""
    questions = 'Какими двумя основными причинами можно объяснить существование экономических проблем?'
    answer = [
        'ограниченность ресурсов для реализации капитальных вложений',
        'влиянием государства на экономику и ростом населения Земли',
        'неограниченностью желаний людей и ограниченностью ресурсов',
        'наличием безработицы и инфляции',
    ]
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=2)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)

    questions = 'Компания утвердила выплату дивидендов, Вы можете на них рассчитывать, если:'
    answer = [
        'Имеете хоть 1 акцию, по которой назначены дивиденды',
        'Имеете 50% + 1 акцию компании',
        'Имеете только привилегированные акции',
        'У Вас открыто ИИС',
    ]
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=0)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)

    questions = 'Капитальные вложения представляют собой:'
    answer = [
        'вложения в оборотный капитал',
        'вложения в основной капитал',
        'вложения в ценные бумаги',
        'вложения в нематериальные активы',
    ]
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=1)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)


def IT(update, context):
    """Отправка заранее определенную викторину"""
    questions = 'Как называется набор однотипных данных, имеющий общее для всех своих элементов имя?'
    answer = [
        'множество',
        'массив',
        'запись',
        'цикл',
    ]
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=1)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)

    questions = ' Первый стандарт ассоциации по языкам обработки данных назывался '
    answer = [
        'SQL',
        'IMS',
        'IBM',
        'CODASYL',
    ]
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=3)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)

    questions = ' Для выборки записей и обновления данных из одной или нескольких таблиц базы данных служат:'
    answer = [
        'отчёты',
        'формы',
        'запросы',
        'таблицы',
    ]
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=2)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)


def eng(update, context):
    """Отправка заранее определенную викторину"""
    questions = 'Какое из следующих слов, является наиболее широко используемым английским словом во всем мире?'
    answer = ['dollar', 'okay', 'internet', 'movie']
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=1)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)

    questions = ' You can by fish at ______.'
    answer = ["fishmonger's shop", "fishmonger's", 'fishmonger shop', 'shop fishmonger']
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=1)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)

    questions = ' Mrs John ________ German when she is in a hurry.'
    answer = ['is speaking', 'speaks', 'is always speaking', 'are speaking']
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=2)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)


def eco(update, context):
    """Отправка заранее определенную викторину"""
    questions = 'Какое озеро самое большое в мире?'
    answer = ['Каспийское море', 'Байкал', 'Гурон', 'Ладожское']
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=0)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)

    questions = 'Кого из перечисленных ученых считают создателем эволюционного учения?'
    answer = ['И.И. Мечникова', 'Луи Пастера ', 'Н.И. Вавилова', 'Ч. Дарвина']
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=3)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)

    questions = (
        ' Индивидуальное развитие любого организма от момента оплодотворения до завершенияжизнедеятельности - это '
    )
    answer = ['филогенез', 'онтогенез', 'эмбриогенез', 'этноламоногенез']
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=1)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)


def avto(update, context):
    """Отправка заранее определенную викторину"""
    questions = 'Что означает Фольксваген в переводе с немецкого?'
    answer = ['удобный автомобиль', 'автомобиль для людей', 'крылья бабочки', 'автомобиль будущего']
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=1)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)

    questions = 'По одной из версий название сложилось из слов роскошь и элегантность. По другой это акроним фразы, перевод которой означает что это машины для внутреннего рынка США'
    answer = ['Lexus', 'Infiniti', 'Maybach', 'Ascari']
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=0)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)

    questions = 'По какой причине появились кабриолеты на базе Москвича и Победы?'
    answer = [
        'из-за нехватки стального листа',
        'для экспорта в жаркие страны',
        'из-за особенностей климата в СССР',
        'для участия в парадах',
    ]
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=0)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)


def sport(update, context):
    """Отправка заранее определенную викторину"""
    questions = 'В какой игре от каждой команды на площадке одновременно присутствует пять игроков?'
    answer = ['баскетбол', 'волейбол', 'футбол', 'водное поло']
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=0)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)

    questions = 'Название какой командной спортивной игры произошло от сочетания «удар с лёта»?'
    answer = ['баскетбол', 'волейбол', 'футбол', 'водное поло']
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=1)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)

    questions = 'Как называется зимний олимпийский вид спорта — скоростной спуск с горы по специально подготовленным ледовым трассам на управляемых санях?'
    answer = ['Скелетон', 'Натурбан', 'Бобслей', 'Биатлон']
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=0)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)


def matan(update, context):
    """Отправка заранее определенную викторину"""
    questions = 'Плата за телефон составляет 350 рублей в месяц. В следующем году она увеличится на 12%. Сколько рублей придётся платить ежемесячно за телефон в следующем году?'
    answer = ['42 рубля', '442 рубля', '932 рубля', '392 рубля']
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=3)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)

    questions = 'Простое число делится на…'
    answer = ['Себя и единицу', 'Себя и 5', 'Себя и 2', 'еденицу и 10']
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=0)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)

    questions = 'Минимальное кол-во прямых углов в прямоугольной трапеции'
    answer = ['1', '2', '3', '4']
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=1)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)


def tehno_it(update, context):
    """Отправка заранее определенную викторину"""
    questions = 'Какой тип процессора чаще всего используют мобильные девайсы?'
    answer = [
        'iOS использует Intel, остальные используют AMD',
        'Чаще всего используют Intel',
        'Чаще всего используют AMD',
        'Чаще всего используют ARM',
    ]
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=3)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)

    questions = 'Качество изображения монитора определяется'
    answer = [
        'способом подключения',
        'разрешающей способностью',
        'типом видеокарты',
        'его размерами',
    ]
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=3)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)

    questions = 'Пути повышения производительности процессора заключаются в'
    answer = [
        'В совершенствовании архитектуры, ввод кэш-памяти, использовании нескольких ядер',
        'в изменении функциональной схемы',
        'в увеличении числа разъемов процессора',
        'в определённых секторах',
    ]
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=3)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)


def fem(update, context):
    """Отправка заранее определенную викторину"""
    questions = 'Сколько феминисток необходимо, чтобы поменять лампочку'
    answer = ['10', '20', '30', 'Феменистки не смогут ничего поменять']
    message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=3)
    payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
    context.bot_data.update(payload)


def venera_jupyter():
    questions = 'Какой это трек?'
    answer = [
        'HammAli & Navai - Птичка',
        'Dabro - На часах ноль-ноль',
        'Ваня Дмитриенко - Венера-Юпитер',
        'HENSY - Поболело и прошло',
    ]
    return [questions, answer, 2]


def junost():
    questions = 'Какой это трек?'
    answer = ['Макс Корж - шантаж', 'Dabro - Юность', 'Ваня Дмитриенко - 36.3', 'Нервы - слишком влюблён']
    return [questions, answer, 1]


def sijay():
    questions = 'Какой это трек?'
    answer = ['Rauf & Faik - Детство', 'Ramil - Сияй', 'The Limba - Смузи', 'Егор Крид - Голубые глаза']
    return [questions, answer, 1]


def krash():
    questions = 'Какой это трек?'
    answer = [
        'Дора, МЭЙБИ БЭЙБИ - Не исправлюсь',
        'Karna.val - Психушка',
        'Алена Швец — Одуванчик',
        'Клава Кока - Краш',
    ]
    return [questions, answer, 3]


def dulo():
    questions = 'Какой это трек?'
    answer = [
        'MORGENSHTERN - Дуло',
        'MORGENSHTERN, Элджей - Cadillac',
        'MORGENSHTERN, Тимати - El Problema',
        'MORGENSHTERN - Cristal & МОЕТ',
    ]
    return [questions, answer, 0]


def dva_tipa_liudey():
    questions = 'Какой это трек?'
    answer = ['MACAN - Кино', 'Канги - Эйя', 'Баста - Сансара', 'Макс Корж - 2 типа людей']
    return [questions, answer, 3]

    # message = update.effective_message.reply_poll(questions, answer, type=Poll.QUIZ, correct_option_id=3)
    # payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
