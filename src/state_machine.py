import datetime
import os
import re
from random import choice

import requests

from src.db.database import DB_new, DB_search
from src.inline_buttons import currency, game, quiz_keyboard, settings, time_zone_buttons, work
from src.modules import compliment_generator, currencies, get_weather, memes
from src.quiz import dulo, dva_tipa_liudey, junost, krash, sijay, venera_jupyter
from src.tasks import send_music
from src.travel import get_city_desc
from src.trello import get_trello_data


class STATE:
    def __init__(self, tg_id: int, message: str, last_state: str) -> None:
        self.tg_id: int = tg_id
        self.message: str = message
        self.last_state: str = last_state


DBN = DB_new()
DBS = DB_search()
weater_template = re.compile(r'[хочу|выведи|покажи|дай] погоду')
trello_template = re.compile(r'[выведи|покажи] задания')
travel_template = re.compile(r'[спланируй] поездку')
memes_template = re.compile(r'мемы')
currency_template = re.compile(r'валюты')
compliment_template = re.compile(r'комплименты')
quiz_template = re.compile(r'[хочу|выведи|покажи|дай] квиз')
music_quiz_template = re.compile(r'[начни|запусти] музыкальную викторину')
work_template = re.compile(r'хочу производительность')
game_template = re.compile(r'хочу развлечения')


def state_handler(state_obj: STATE) -> dict:
    if state_obj.message.lower() == 'настройки':
        DBN.set_state(state_obj.tg_id, 'base')
        state_obj.last_state = 'base'

    if state_obj.last_state == 'base':
        state_obj.message = state_obj.message.lower()

        if music_quiz_template.findall(state_obj.message):
            current_music = choice(['Венера-Юпитер', 'Два-типа-людей', 'Дуло', 'Краш', 'Сияй', 'Юность'])
            if current_music == 'Венера-Юпитер':
                send_music.send_with_options(
                    args=[f'./src/music_quiz/{current_music}.mp3', state_obj.tg_id], delay=180000
                )
                return dict(
                    message=['Держи реверс трэка. Угадай. \n Через 3 минуты пришлю ответ'],
                    poll=[venera_jupyter()],
                    music=[f'./src/music_quiz/{current_music}-reverse.mp3'],
                )
            elif current_music == 'Два-типа-людей':
                send_music.send_with_options(
                    args=[f'./src/music_quiz/{current_music}.mp3', state_obj.tg_id], delay=180000
                )
                return dict(
                    message=['Держи реверс трэка. Угадай. \n Через 3 минуты пришлю ответ'],
                    poll=[dva_tipa_liudey()],
                    music=[f'./src/music_quiz/{current_music}-reverse.mp3'],
                )
            elif current_music == 'Дуло':
                send_music.send_with_options(
                    args=[f'./src/music_quiz/{current_music}.mp3', state_obj.tg_id], delay=180000
                )
                return dict(
                    message=['Держи реверс трэка. Угадай. \n Через 3 минуты пришлю ответ'],
                    poll=[dulo()],
                    music=[f'./src/music_quiz/{current_music}-reverse.mp3'],
                )
            elif current_music == 'Краш':
                send_music.send_with_options(
                    args=[f'./src/music_quiz/{current_music}.mp3', state_obj.tg_id], delay=180000
                )
                return dict(
                    message=['Держи реверс трэка. Угадай. \n Через 3 минуты пришлю ответ'],
                    poll=[krash()],
                    music=[f'./src/music_quiz/{current_music}-reverse.mp3'],
                )
            elif current_music == 'Сияй':
                send_music.send_with_options(
                    args=[f'./src/music_quiz/{current_music}.mp3', state_obj.tg_id], delay=180000
                )
                return dict(
                    message=['Держи реверс трэка. Угадай. \n Через 3 минуты пришлю ответ'],
                    poll=[sijay()],
                    music=[f'./src/music_quiz/{current_music}-reverse.mp3'],
                )
            elif current_music == 'Юность':
                send_music.send_with_options(
                    args=[f'./src/music_quiz/{current_music}.mp3', state_obj.tg_id], delay=180000
                )
                return dict(
                    message=['Держи реверс трэка. Угадай. \n Через 3 минуты пришлю ответ'],
                    poll=[junost()],
                    music=[f'./src/music_quiz/{current_music}-reverse.mp3'],
                )

        if weater_template.findall(state_obj.message):
            city = DBS.get_city(state_obj.tg_id)
            if city:
                DBN.set_state(state_obj.tg_id, 'base')
                return get_weather(DBS.get_city_name(city))
            else:
                DBN.set_state(state_obj.tg_id, 'weather_wait_city')
                return dict(message=['Введите город в котором проживаете'])

        elif trello_template.findall(state_obj.message):
            trello = DBS.get_trello(state_obj.tg_id)
            if trello is None:
                DBN.set_state(state_obj.tg_id, 'trello_wait_id')
                return dict(message=['Cообщите ID Trello доски или ссылку на доску'])
            else:
                DBN.set_state(state_obj.tg_id, 'base')
                return dict(markdown=get_trello_data(trello))

        elif travel_template.findall(state_obj.message):
            DBN.set_state(state_obj.tg_id, 'travel_wait_city')
            return dict(message=['Введите город, в которой планируете путешествие'])

        elif memes_template.findall(state_obj.message):
            return memes()

        elif compliment_template.findall(state_obj.message):
            return compliment_generator()

        elif state_obj.message == 'curr_is_ready':
            DBN.set_state(state_obj.tg_id, 'base')
            return currencies(DBS.get_currency(state_obj.tg_id))

        elif currency_template.findall(state_obj.message):
            curr = DBS.get_currency(state_obj.tg_id)
            if curr:
                curr_res = currencies(curr)
                DBN.set_state(state_obj.tg_id, 'base')
                if curr_res is None:
                    return dict(message=['Ошибка при получении данных'])
                return curr_res
            else:
                DBN.set_state(state_obj.tg_id, 'currency_wait_curr')
                return dict(message=['Какие валюты вы бы хотели увидеть?'], buttons=currency())

        elif quiz_template.findall(state_obj.message):
            return dict(buttons=quiz_keyboard(), message=['Выберите категорию викторины'])

        elif work_template.findall(state_obj.message):
            return dict(buttons=work(), message=['Выберите необходимый функционал:'])

        elif game_template.findall(state_obj.message):
            return dict(buttons=game(), message=['Во что вы хотите сыграть?'])

        elif state_obj.message == 'настройки':
            return dict(buttons=settings(), message=['Настройки'])

    elif state_obj.last_state == 'currency_wait_curr':
        if state_obj.message == 'curr_is_ready':
            DBN.set_state(state_obj.tg_id, 'base')
            return currencies(DBS.get_currency(state_obj.tg_id))
        return dict(message=["Выбери нужные валюты, потом нажми 'Готово'"])

    elif state_obj.last_state == 'weather_wait_city':
        weather_out = get_weather(state_obj.message)
        if weather_out is None:
            return dict(message=['Такого города не существует, повтори ещё раз'])
        else:
            DBN.set_city(state_obj.tg_id, state_obj.message)
            DBN.set_state(state_obj.tg_id, 'base')
            return weather_out

    elif state_obj.last_state == 'trello_wait_id':
        if 'http' in state_obj.message:
            trello_id = state_obj.message.split('/')[4]
        else:
            trello_id = state_obj.message
        trello_out = get_trello_data(trello_id)
        if trello_out is None:
            return dict(message=['Проверь ID доски или сделай её общедоступной'])
        else:
            DBN.set_trello(state_obj.tg_id, trello_id)
            DBN.set_state(state_obj.tg_id, 'base')
            return dict(markdown=trello_out)

    elif state_obj.last_state == 'travel_wait_city':
        city_desc = get_city_desc(state_obj.message)
        if city_desc is None:
            return dict(message=['Такого города нет в базе данных, пожалуйста, проверьте правильность написания'])
        DBN.set_state(state_obj.tg_id, 'base')
        return dict(markdown=city_desc)

    elif state_obj.last_state == 'subscription_wait_time':
        time = date_time_prepare(state_obj.message)
        if time is None:
            return dict(message=['Неправильный формат времени. Повтори ещё раз'])
        DBN.set_sub_time(state_obj.tg_id, time)
        DBN.set_state(state_obj.tg_id, 'base')
        return dict(message=['Выбери часовой пояс относительно Москвы'], buttons=time_zone_buttons())

    return dict(stickers=['base'], message=base_state(state_obj.message), is_recognize=True)


def date_time_prepare(delay: str):
    try:
        needed_time = datetime.datetime.strptime(delay, "%H:%M")
        needed_time = datetime.datetime.now().replace(
            hour=needed_time.hour, minute=needed_time.minute, second=00, microsecond=00
        )
        return needed_time
    except:
        return None


def base_state(message) -> str:
    return [
        requests.post(
            'https://dialogue-as-a-service.toys.dialogic.digital/boltalka_as_a_service',
            json={'utterance': message, 'token': os.getenv('TINKOFF_BASE_STATE_TOKEN')},
        ).json()['response']
    ]
