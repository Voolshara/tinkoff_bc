import datetime
import os

import dramatiq
from dramatiq.brokers.redis import RedisBroker
from redis.sentinel import Sentinel
from telegram import ParseMode
from telegram.bot import Bot

from src.db.database import DB_new, DB_search
from src.modules import currencies, get_weather
from src.trello import get_trello_data

sentinel = Sentinel([(os.getenv('TINKOFF_REDIS_HOST'), 26379)], socket_timeout=3)
master = sentinel.master_for(os.getenv('TINKOFF_REDIS_CLUSTER_NAME'), password=os.getenv('TINKOFF_REDIS_PASSWORD'))
redis_broker = RedisBroker(client=master)
dramatiq.set_broker(redis_broker)

# redis_broker = RedisBroker(host="{}".format(os.getenv('TINKOFF_REDIS_HOST')))
# dramatiq.set_broker(redis_broker)

DBN = DB_new()
DBS = DB_search()


def count_delay(task_time: datetime.datetime, zone: int) -> int:
    rigth_now = datetime.datetime.utcnow()
    task_time += datetime.timedelta(hours=zone)
    if task_time < rigth_now:
        task_time += datetime.timedelta(days=1)
    diff = task_time - rigth_now
    return int((diff.seconds * 1000) + (diff.microseconds / 1000))


@dramatiq.actor
def trello_dramatiq(task_id: int):
    task = DBS.get_task_data(task_id)
    if task is None or task[2] == False:
        return
    r = Bot('{}'.format(os.getenv('TINKOFF_TG_TOKEN')))
    trello = DBS.get_trello(task[0])
    r.send_message(task[0], get_trello_data(trello)[0], parse_mode=ParseMode.MARKDOWN_V2)
    trello_dramatiq.send_with_options(args=[task_id], delay=count_delay(task[3], task[4]))


@dramatiq.actor
def weather_dramatiq(task_id: int):
    task = DBS.get_task_data(task_id)
    if task is None or task[2] == False:
        return
    r = Bot('{}'.format(os.getenv('TINKOFF_TG_TOKEN')))
    city = DBS.get_city(task[0])
    r.send_message(task[0], get_weather(DBS.get_city_name(city))['message'][0])
    weather_dramatiq.send_with_options(args=[task_id], delay=count_delay(task[3], task[4]))


@dramatiq.actor
def curr_dramatiq(task_id: int):
    task = DBS.get_task_data(task_id)
    if task is None or task[2] == False:
        return
    r = Bot('{}'.format(os.getenv('TINKOFF_TG_TOKEN')))
    curr = DBS.get_currency(task[0])
    curr_res = currencies(curr)
    r.send_message(task[0], curr_res['markdown'][0], parse_mode=ParseMode.MARKDOWN_V2)
    weather_dramatiq.send_with_options(args=[task_id], delay=count_delay(task[3], task[4]))


@dramatiq.actor
def send_music(music_path: str, tg_id: int):
    with open(music_path, 'rb') as music_file:
        r = Bot('{}'.format(os.getenv('TINKOFF_TG_TOKEN')))
        r.send_message(tg_id, 'Ответ на Музыкальную Викторину')
        r.send_voice(tg_id, music_file.read())
