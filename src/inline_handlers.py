import requests, os
import random

from telegram import Update
from telegram.ext import CallbackContext

from src.db.database import DB_new, DB_search
from src.handlers import send_out
from src.inline_buttons import currency, subscriptions_setup, voices, news
from src.quiz import IT, avto, eco, economic, eng, matan, sport, tehno_it
from src.state_machine import STATE, state_handler
from src.tasks import count_delay, curr_dramatiq, trello_dramatiq, weather_dramatiq
from src.modules import ALL_STICKERS 

DBN = DB_new()
DBS = DB_search()


def buttons_headlers(update: Update, context: CallbackContext) -> None:
    query = update.callback_query
    query.answer()

    if update.effective_chat is not None:
        now_id = update.effective_chat.id
    else:
        now_id = update.effective_user.id

    # This will define which button the user tapped on (from what you assigned to "callback_data". As I assigned them "1" and "2"):
    choice = query.data

    if choice == "Travel":
        DBN.set_state(now_id, 'base')
        query.message.reply_sticker(ALL_STICKERS['travel'])
        out_data = state_handler(STATE(now_id, 'спланируй поездку', 'base'))
        send_out(update, out_data, False)

    elif choice == "Feedback":
        DBN.set_state(now_id, 'base')
        query.message.reply_sticker(ALL_STICKERS['news'])
        query.message.reply_text("Расскажи, что ты думаешь о наешм продукте?")

    elif choice == "News_Plugins":
        query.message.reply_sticker(ALL_STICKERS['news'])
        query.message.reply_text("Выберите тему новости", reply_markup=news())

    elif choice == "Wheather":
        DBN.set_state(now_id, 'base')
        query.message.reply_sticker(ALL_STICKERS['think'])
        out_data = state_handler(STATE(now_id, 'хочу погоду', 'base'))
        send_out(update, out_data, False)

    elif choice == "Currency":
        DBN.set_state(now_id, 'base')
        query.message.reply_sticker(ALL_STICKERS['think'])
        out_data = state_handler(STATE(now_id, 'валюты', 'base'))
        send_out(update, out_data, False)

    elif choice == 'Settings':
        DBN.set_state(now_id, 'base')
        query.message.reply_sticker(ALL_STICKERS['setting'])
        out_data = state_handler(STATE(now_id, 'настройки', 'base'))
        send_out(update, out_data, False)

    elif choice == 'Settings_trello':
        query.message.reply_text("Cообщите ID Trello доски или ссылку на доску")
        DBN.set_state(now_id, 'trello_wait_id')

    elif choice == 'Settings_city':
        query.message.reply_text("Введите город в котором проживаете")
        DBN.set_state(now_id, 'weather_wait_city')

    elif choice == 'Settings_curr':
        query.message.reply_text("Какие валюты вы бы хотели увидеть?", reply_markup=currency())
        DBN.delete_currency(now_id)
        DBN.set_state(now_id, 'currency_wait_curr')

    elif choice == 'Settings_voice':
        query.message.reply_text("Выберете голос", reply_markup=voices())

    elif choice == 'Alyona_voice_setup':
        DBN.set_voice(now_id, 'alyona')
        query.message.reply_text('Голос установлен')

    elif choice == 'Maxim_voice_setup':
        DBN.set_voice(now_id, 'maxim')
        query.message.reply_text('Голос установлен')

    elif choice == 'Flirting_alyona_voice_setup':
        DBN.set_voice(now_id, 'flirt')
        query.message.reply_text('Голос установлен')

    elif choice == "Dollar":
        DBN.set_currency(now_id, 'USD')
        query.message.reply_text("Доллары добавлены")

    elif choice == "Euro":
        DBN.set_currency(now_id, 'EUR')
        query.message.reply_text("Евро добавлено")

    elif choice == "Yen":
        DBN.set_currency(now_id, 'CNY')
        query.message.reply_text("Юани добавлены")

    elif choice == "Pounds":
        DBN.set_currency(now_id, 'GBP')
        query.message.reply_text("Фунты добавлены")

    elif choice == "Curr_ready":
        now_state = DBS.get_state(now_id)
        query.message.reply_sticker(ALL_STICKERS['think'])        
        out_data = state_handler(STATE(now_id, 'curr_is_ready', now_state))
        send_out(update, out_data, False)

    elif choice == "TO DO":
        DBN.set_state(now_id, 'base')
        out_data = state_handler(STATE(now_id, 'выведи задания', 'base'))
        send_out(update, out_data, False)

    elif choice == "Quiz":
        DBN.set_state(now_id, 'base')
        out_data = state_handler(STATE(now_id, 'хочу квиз', 'base'))
        send_out(update, out_data, False)

    elif choice == "works":
        DBN.set_state(now_id, 'base')
        query.message.reply_sticker(ALL_STICKERS['work'])        
        out_data = state_handler(STATE(now_id, 'хочу производительность', 'base'))
        send_out(update, out_data, False)

    elif choice == "games":
        DBN.set_state(now_id, 'base')
        query.message.reply_sticker(ALL_STICKERS['game'])        
        out_data = state_handler(STATE(now_id, 'хочу развлечения', 'base'))
        send_out(update, out_data, False)

    elif choice == "Music_quiz":
        DBN.set_state(now_id, 'base')
        out_data = state_handler(STATE(now_id, 'запусти музыкальную викторину', 'base'))
        send_out(update, out_data, False)

    elif choice == 'category_econom':
        economic(update, context)

    elif choice == 'category_it':
        IT(update, context)

    elif choice == 'category_eng':
        eng(update, context)

    elif choice == 'category_ecology':
        eco(update, context)

    elif choice == 'category_avto':
        avto(update, context)

    elif choice == 'category_sport':
        sport(update, context)

    elif choice == 'category_matan':
        matan(update, context)

    elif choice == 'category_tehno':
        tehno_it(update, context)

    elif choice == 'Subscriptions':
        query.message.reply_text('Какие уведомления вы хотите получать?', reply_markup=subscriptions_setup())

    elif choice == 'sub_weather':
        DBN.set_sub(now_id, 'weather')
        query.message.reply_text("Вы подписались на погоду")

    elif choice == 'sub_curr':
        DBN.set_sub(now_id, 'curr')
        query.message.reply_text("Вы подписались на валюты")

    elif choice == 'sub_news':
        DBN.set_sub(now_id, 'news')
        query.message.reply_text("Вы подписались на новости")

    elif choice == 'sub_trello':
        DBN.set_sub(now_id, 'trello')
        query.message.reply_text("Вы подписались на trello")

    elif choice == 'Subscription_ready':
        query.message.reply_sticker(ALL_STICKERS['think'])        
        query.message.reply_text("Введите время, в которое хоите получать уведомления \n в формате 08:30")
        DBN.set_state(now_id, 'subscription_wait_time')

    elif choice == 'Subscription_delete':
        query.message.reply_sticker(ALL_STICKERS['delete'])        
        DBN.delete_sub(now_id)
        query.message.reply_text("Вы отписались от всех подписок")

    elif choice == 'msk+0':
        DBN.set_sub_zone(now_id, -3)
        query.message.reply_text("Часовой пояс установлен")
        setup_all_tasks(now_id)

    elif choice == 'msk-1':
        DBN.set_sub_zone(now_id, -2)
        query.message.reply_text("Часовой пояс установлен")
        setup_all_tasks(now_id)

    elif choice == 'msk+1':
        DBN.set_sub_zone(now_id, -4)
        query.message.reply_text("Часовой пояс установлен")
        setup_all_tasks(now_id)

    elif choice == 'msk+2':
        DBN.set_sub_zone(now_id, -5)
        query.message.reply_text("Часовой пояс установлен")
        setup_all_tasks(now_id)

    elif choice == 'msk+3':
        DBN.set_sub_zone(now_id, -5)
        query.message.reply_text("Часовой пояс установлен")
        setup_all_tasks(now_id)

    elif choice == 'msk+4':
        DBN.set_sub_zone(now_id, -6)
        query.message.reply_text("Часовой пояс установлен")
        setup_all_tasks(now_id)

    elif choice == 'msk+5':
        DBN.set_sub_zone(now_id, -7)
        query.message.reply_text("Часовой пояс установлен")
        setup_all_tasks(now_id)

    elif choice == 'msk+5':
        DBN.set_sub_zone(now_id, -7)
        query.message.reply_text("Часовой пояс установлен")
        setup_all_tasks(now_id)

    elif choice == 'msk+6':
        DBN.set_sub_zone(now_id, -8)
        query.message.reply_text("Часовой пояс установлен")
        setup_all_tasks(now_id)

    elif choice == 'msk+7':
        DBN.set_sub_zone(now_id, -9)
        query.message.reply_text("Часовой пояс установлен")
        setup_all_tasks(now_id)

    elif choice == 'msk+8':
        DBN.set_sub_zone(now_id, -10)
        query.message.reply_text("Часовой пояс установлен")
        setup_all_tasks(now_id)

    elif choice == 'msk+9':
        DBN.set_sub_zone(now_id, -11)
        query.message.reply_text("Часовой пояс установлен")
        setup_all_tasks(now_id)

    elif choice in ["news_general", "news_tech", "news_business", "news_entertainment",
                    "news_sport", "news_health", "news_science"]:
        query.message.reply_sticker(ALL_STICKERS['2_hands'])        
        acc = []
        r = requests.get(f"https://newsapi.org/v2/top-headlines?country=ru&category={choice[5:]}&apiKey={os.getenv('TINKOFF_NEWS_KEY')}")
        data = r.json()
        for i in range(len(data["articles"])):
            acc.append(data["articles"][i]["url"])
        for i in range(3):
            query.message.reply_text(random.choice(acc))

    elif choice == 'Memes':
        now_state = DBS.get_state(now_id)
        out_data = state_handler(STATE(now_id, 'мемы', now_state))
        send_out(update, out_data, False)

    elif choice == 'Coin_Flip':
        coin_states = ['Решка', 'Орёл']
        send_out(update, dict(message=[random.choice(coin_states)]), True)

    elif choice == 'Compliments':
        query.message.reply_sticker(ALL_STICKERS['news'])
        out_data = state_handler(STATE(now_id, 'комплименты', 'base'))
        send_out(update, out_data, True)

def setup_all_tasks(tg_id: int):
    ALL_TASKS = DBS.get_user_sub(tg_id)
    for task_id, telegram_id, task_name, time, zone in ALL_TASKS:
        # print(task_name, count_delay(time, zone))
        if task_name == 'trello':
            trello_dramatiq.send_with_options(args=[task_id], delay=count_delay(time, zone))
        elif task_name == 'weather':
            weather_dramatiq.send_with_options(args=[task_id], delay=count_delay(time, zone))
        elif task_name == 'curr':
            curr_dramatiq.send_with_options(args=[task_id], delay=count_delay(time, zone))
