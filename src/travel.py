def get_city_desc(city: str) -> None:
    if city.lower() in {'чебоксары', "чебы"}:
        return [
            "[Научно технический музей истории трактора](https://clck.ru/W8w4k)",
            '[Монумент Матери Покровительнице](https://www.tripadvisor.ru/Attraction_Review-g298481-d6426155-Reviews-Monument_Materi_Pokrovitelnitse-Cheboksary_Chuvash_Republic_Volga_District.html)',
            '[Мемориальный Парк Победа](https://www.tripadvisor.ru/Attraction_Review-g298481-d6851663-Reviews-Memorial_Park_Pobeda-Cheboksary_Chuvash_Republic_Volga_District.html)',
            '[Музей Пива](https://www.tripadvisor.ru/Attraction_Review-g298481-d2553427-Reviews-Beer_Museum-Cheboksary_Chuvash_Republic_Volga_District.html)',
            '[Чебоксарский залив](https://www.tripadvisor.ru/Attraction_Review-g298481-d10378784-Reviews-Cheboksary_Bay-Cheboksary_Chuvash_Republic_Volga_District.html)',
        ]

    elif city.lower() in {'москва', "мск", "москов"}:
        return [
            "[Десять лучших парков Москвы](https://clck.ru/W8em8)",
            '[Ночные прогулки по центру](https://www.tripadvisor.ru/ShowUserReviews-g298484-d300366-r531898165-Red_Square-Moscow_Central_Russia.html)',
            '[Храм Василия Блаженного](https://zen.yandex.ru/media/manikol/hram-vasiliia-blajennogo-v-moskve-a-tochnee-pokrovskii-sobor-iz-11-cerkvei--chto-interesnogo-vnutri-609d01b41a7ddc239380c6ed)',
            "[Эксперементаниум](https://experimentanium.ru/)",
            '[Государственный Исторический Музей](https://shm.ru/#)',
        ]

    elif city.lower() in {
        'санкт-петербург',
        "санкт петербург",
        "петербург",
        "питер",
        "петер",
        "питербург",
        "спб",
        "солевая столица",
    }:
        return [
            "[Парк и сады Петергофа](https://clck.ru/W8iyt)",
            '[Государственный Эрмитаж](https://clck.ru/W8j2Q)',
            '[Музей Фаберже](https://clck.ru/W8jBj)',
            '[Зеленогородский парк культуры и отдыха](https://clck.ru/W8jTF)',
            '[Памятник Петру Первому основателю Кронштадта](https://clck.ru/W8jXw)',
        ]

    elif city.lower() in {'казань', "казан", "казанка"}:
        return [
            "[Памятник Коту Казанскому](https://www.tripadvisor.ru/Attraction_Review-g298520-d8843611-Reviews-Monument_Cat_Kazan-Kazan_Republic_of_Tatarstan_Volga_District.html)",
            '[Kazan Kremlin](https://www.tripadvisor.ru/Attraction_Review-g298520-d321110-Reviews-Kazan_Kremlin-Kazan_Republic_of_Tatarstan_Volga_District.html)',
            '[Парк Тысячелетия Казани](https://www.tripadvisor.ru/Attraction_Review-g298520-d4570167-Reviews-Millennium_Park_of_Kazan-Kazan_Republic_of_Tatarstan_Volga_District.html)',
            '[Центральный парк культуры и отдыха имени Горького](https://www.tripadvisor.ru/Attraction_Review-g298520-d8680359-Reviews-Gorkiy_Central_Park_of_Culture_and_Recreation-Kazan_Republic_of_Tatarstan_Volga_D.html)',
            '[Музей Чак-чака](https://www.tripadvisor.ru/Attraction_Review-g298520-d9754996-Reviews-Museum_of_Chak_Chak-Kazan_Republic_of_Tatarstan_Volga_District.html)',
        ]

    elif city.lower() in {'екатеринбург', "екб", "ёбург", "ебург"}:
        return [
            "[Плотина Городского пруда на реке Исеть](https://www.tripadvisor.ru/Attraction_Review-g298540-d2331791-Reviews-Weir_on_river_Iset-Yekaterinburg_Sverdlovsk_Oblast_Urals_District.html)",
            '[Ельцин Центр](https://yeltsin.ru/)',
            '[Екатеринбургский дендропарк](https://www.tripadvisor.ru/Attraction_Review-g298540-d7137508-Reviews-Dendropark-Yekaterinburg_Sverdlovsk_Oblast_Urals_District.html)',
            '[Здание главного учебного корпуса УрФУ](https://2gis.ru/ekaterinburg/geo/70030076142556904)',
            '[Екатеринбург Арена](https://2gis.ru/ekaterinburg/geo/70030076161393309)',
        ]

    elif city.lower() in {'сочи'}:
        return [
            "[Имеретинская набережная](https://www.tripadvisor.ru/Attraction_Review-g298536-d8532142-Reviews-Imeretinskaya_Embankment-Sochi_Greater_Sochi_Krasnodar_Krai_Southern_District.html)",
            '[Светомузыкальный фонтан](https://www.tripadvisor.ru/Attraction_Review-g298536-d2333873-Reviews-The_Singing_Fountains-Sochi_Greater_Sochi_Krasnodar_Krai_Southern_District.html)',
            '[Морская набережная](https://www.tripadvisor.ru/Attraction_Review-g298536-d9765696-Reviews-Morskaya_Embankment-Sochi_Greater_Sochi_Krasnodar_Krai_Southern_District.html)',
            '[Сочинский маяк](https://www.tripadvisor.ru/Attraction_Review-g298536-d7062405-Reviews-Sochi_Lighthouse-Sochi_Greater_Sochi_Krasnodar_Krai_Southern_District.html)',
            '[Крепость Годлик](https://www.tripadvisor.ru/Attraction_Review-g298536-d2333867-Reviews-Godlik_fortress-Sochi_Greater_Sochi_Krasnodar_Krai_Southern_District.html)',
        ]

    elif city.lower() in {'нижний-новгород', "нижний новгород", "нижний"}:
        return [
            "[Нижегородский кремль](https://www.tripadvisor.ru/Attraction_Review-g298515-d600733-Reviews-The_Kremlin-Nizhny_Novgorod_Nizhny_Novgorod_Oblast_Volga_District.html)",
            '[Набережная Федоровского](https://www.tripadvisor.ru/Attraction_Review-g298515-d3578054-Reviews-Fedorovsky_Embankment-Nizhny_Novgorod_Nizhny_Novgorod_Oblast_Volga_District.html)',
            '[Чкаловская лестница](https://www.tripadvisor.ru/Attraction_Review-g298515-d305008-Reviews-Chkalov_Staircase-Nizhny_Novgorod_Nizhny_Novgorod_Oblast_Volga_District.html)',
            '[Государственный банк](https://www.tripadvisor.ru/Attraction_Review-g298515-d8422795-Reviews-State_Bank-Nizhny_Novgorod_Nizhny_Novgorod_Oblast_Volga_District.html)',
            '[Нижне Волжская набережная](https://www.tripadvisor.ru/Attraction_Review-g298515-d6779128-Reviews-Nizhne_Volzhskaya_Embankment-Nizhny_Novgorod_Nizhny_Novgorod_Oblast_Volga_Distric.html#REVIEWS)',
        ]

    elif city.lower() in {"калининград"}:
        return [
            "[Кафедральный собор](https://www.tripadvisor.ru/Attraction_Review-g298500-d544640-Reviews-Konigsberg_Cathedral-Kaliningrad_Kaliningrad_Oblast_Northwestern_District.html)",
            '[Могила Иммануила Канта](https://www.tripadvisor.ru/Attraction_Review-g298500-d544624-Reviews-Immanuel_Kant_s_Grave-Kaliningrad_Kaliningrad_Oblast_Northwestern_District.html)',
            '[Видовая башня Маяк](https://www.tripadvisor.ru/Attraction_Review-g298500-d7833508-Reviews-Viewing_Tower_Lighthouse-Kaliningrad_Kaliningrad_Oblast_Northwestern_District.html)',
            '[Бранденбургские ворота](https://www.tripadvisor.ru/Attraction_Review-g298500-d2330807-Reviews-Brandenburg_Gate-Kaliningrad_Kaliningrad_Oblast_Northwestern_District.html)',
            '[Королевские ворота](https://www.tripadvisor.ru/Attraction_Review-g298500-d544656-Reviews-King_s_Gates-Kaliningrad_Kaliningrad_Oblast_Northwestern_District.html)',
        ]

    elif city.lower() in {'ярославль'}:
        return [
            "[Набережная Ярославля](https://www.tripadvisor.ru/Attraction_Review-g298488-d2705594-Reviews-Yaroslavl_Embankment-Yaroslavl_Yaroslavl_Oblast_Central_Russia.html)",
            '[Фонтаны на Стрелке](https://www.tripadvisor.ru/Attraction_Review-g298488-d2643084-Reviews-Strelka_Fountains-Yaroslavl_Yaroslavl_Oblast_Central_Russia.html)',
            '[Памятник Медведю](https://www.tripadvisor.ru/Attraction_Review-g298488-d4373331-Reviews-Monument_to_Bear-Yaroslavl_Yaroslavl_Oblast_Central_Russia.html)',
            '[Нулевой километр Золотого Кольца](https://www.tripadvisor.ru/Attraction_Review-g298488-d7709097-Reviews-Kilometre_Zero_of_Golden_Ring-Yaroslavl_Yaroslavl_Oblast_Central_Russia.html)',
            '[Памятник тысячелетию Ярославля](https://www.tripadvisor.ru/Attraction_Review-g298488-d7712440-Reviews-Monument_1000_Years_of_Yaroslavl-Yaroslavl_Yaroslavl_Oblast_Central_Russia.html)',
        ]

    elif city.lower() in {'анапа'}:
        return [
            "[Central Fountain](https://www.tripadvisor.ru/Attraction_Review-g608960-d7237665-Reviews-Central_Fountain-Anapa_Krasnodar_Krai_Southern_District.html)",
            '[Анапский Маяк](https://www.tripadvisor.ru/Attraction_Review-g608960-d2331030-Reviews-The_Lighthouse-Anapa_Krasnodar_Krai_Southern_District.html)',
            '[Кораблик Алые Паруса](https://www.tripadvisor.ru/Attraction_Review-g608960-d12584475-Reviews-Scarlet_Sails_Ship-Anapa_Krasnodar_Krai_Southern_District.html)',
            '[Ласточкины гнёзда](https://www.tripadvisor.ru/Attraction_Review-g608960-d14138851-Reviews-Lastochkiny_Gnyozda-Anapa_Krasnodar_Krai_Southern_District.html)',
            '[Дом Вверх Дном](https://www.tripadvisor.ru/Attraction_Review-g608960-d15114104-Reviews-Dom_Vverkh_Dnom-Anapa_Krasnodar_Krai_Southern_District.html)',
        ]

    elif city.lower() in {'владимир', "владик"}:
        return [
            "[Большая Московская улица](https://www.tripadvisor.ru/Attraction_Review-g445046-d9554333-Reviews-Bolshaya_Moskovskaya_Street-Vladimir_Vladimir_Oblast_Central_Russia.html)",
            '[Памятник владимирской вишне](https://www.tripadvisor.ru/Attraction_Review-g445046-d10261164-Reviews-Monument_to_Vladimir_Cherry-Vladimir_Vladimir_Oblast_Central_Russia.html)',
            '[Владимирский централ](https://www.tripadvisor.ru/Attraction_Review-g445046-d2357353-Reviews-Vladimir_central_prison-Vladimir_Vladimir_Oblast_Central_Russia.html)',
            '[Памятник паровоз](https://www.tripadvisor.ru/Attraction_Review-g445046-d7348664-Reviews-Monument_Locomotive-Vladimir_Vladimir_Oblast_Central_Russia.html)',
            '[Памятник Александру Невскому](https://www.tripadvisor.ru/Attraction_Review-g445046-d10260656-Reviews-Monument_to_Alexandr_Nevskiy-Vladimir_Vladimir_Oblast_Central_Russia.html)',
        ]

    elif city.lower() in {'тула', "туль"}:
        return [
            "[Тульский Государственный Музей Оружия](https://www.tripadvisor.ru/Attraction_Review-g298486-d2572123-Reviews-Tula_State_Museum_of_Weapons-Tula_Tula_Oblast_Central_Russia.html#REVIEWS)",
            '[Тульский кремль](https://www.tripadvisor.ru/Attraction_Review-g298486-d2042151-Reviews-Tula_Kremlin_Museum-Tula_Tula_Oblast_Central_Russia.html)',
            '[Памятник тульскому прянику](https://www.tripadvisor.ru/Attraction_Review-g298486-d7038433-Reviews-Monument_to_Tula_Gingerbread-Tula_Tula_Oblast_Central_Russia.html)',
            '[Тульская набережная](https://www.tripadvisor.ru/Attraction_Review-g298486-d15114277-Reviews-Tulskaya_Embankment-Tula_Tula_Oblast_Central_Russia.html)',
            '[Памятник левше](https://www.tripadvisor.ru/Attraction_Review-g298486-d7987081-Reviews-Monument_to_Lefthanded_Person-Tula_Tula_Oblast_Central_Russia.html)',
        ]

    elif city.lower() in {'краснодар'}:
        return [
            "[Памятник Екатерине Великой](https://www.tripadvisor.ru/Attraction_Review-g298532-d6652988-Reviews-Monument_to_Catherine_the_Great-Krasnodar_Krasnodar_Krai_Southern_District.html)",
            '[Парк Галицкого](https://www.tripadvisor.ru/Attraction_Review-g298532-d14768259-Reviews-Galitskogo_Park-Krasnodar_Krasnodar_Krai_Southern_District.html)',
            '[Городской сад](https://www.tripadvisor.ru/Attraction_Review-g298532-d2331962-Reviews-City_Botanical_Garden-Krasnodar_Krasnodar_Krai_Southern_District.html)',
            '[Сафари парк](https://www.tripadvisor.ru/Attraction_Review-g298532-d3747024-Reviews-Safari_Park-Krasnodar_Krasnodar_Krai_Southern_District.html)',
            '[Стадион ФК Краснодар](https://www.tripadvisor.ru/Attraction_Review-g298532-d11626541-Reviews-Stadium_FC_Krasnodar-Krasnodar_Krasnodar_Krai_Southern_District.html)',
        ]

    elif city.lower() in {'воронеж'}:
        return [
            "[Парк Алые паруса](https://www.tripadvisor.ru/Attraction_Review-g798124-d7790763-Reviews-Park_Alyye_Parusa-Voronezh_Voronezh_Oblast_Central_Russia.html)",
            '[Корабль музей Гото Предестинация](https://www.tripadvisor.ru/Attraction_Review-g798124-d6965281-Reviews-Ship_Museum_Goto_Predestinatsia-Voronezh_Voronezh_Oblast_Central_Russia.html)',
            '[Музей забытой музыки](https://www.tripadvisor.ru/Attraction_Review-g798124-d6491346-Reviews-The_Museum_of_the_Forgotten_Music-Voronezh_Voronezh_Oblast_Central_Russia.html)',
            '[Петровский сквер](https://www.tripadvisor.ru/Attraction_Review-g798124-d8555813-Reviews-Petrovskiy_Park-Voronezh_Voronezh_Oblast_Central_Russia.html)',
            '[Памятник "Ротонда"](https://www.tripadvisor.ru/Attraction_Review-g798124-d13130514-Reviews-Monument_Rotonda-Voronezh_Voronezh_Oblast_Central_Russia.html)',
        ]

    elif city.lower() in {'суздаль'}:
        return [
            "[Дом Съемок Фильма Женитьба Бальзаминова](https://www.tripadvisor.ru/Attraction_Review-g580269-d5004024-Reviews-The_Marriage_of_Bal_zaminova_Filming_House-Suzdal_Suzdalsky_District_Vladimir_Obl.html)",
            '[Памятник Алексею Лебедеву](https://www.tripadvisor.ru/Attraction_Review-g580269-d8041351-Reviews-Aleksey_Lebedev_Statue-Suzdal_Suzdalsky_District_Vladimir_Oblast_Central_Russia.html)',
            '[Дом купца Агапова](https://www.tripadvisor.ru/Attraction_Review-g580269-d9861596-Reviews-House_of_Merchant_Agapov-Suzdal_Suzdalsky_District_Vladimir_Oblast_Central_Russia.html)',
            '[Памятник Д.М. Пожарскому](https://www.tripadvisor.ru/Attraction_Review-g580269-d9974277-Reviews-Monument_to_Pozharskiy-Suzdal_Suzdalsky_District_Vladimir_Oblast_Central_Russia.html#REVIEWS)',
            '[Посадский дом](https://www.tripadvisor.ru/Attraction_Review-g580269-d4945287-Reviews-Posad_House-Suzdal_Suzdalsky_District_Vladimir_Oblast_Central_Russia.html)',
        ]

    elif city.lower() in {'новосибирск', "новосиб"}:
        return [
            "[Новосибирский зоопарк](https://www.tripadvisor.ru/Attraction_Review-g298529-d307741-Reviews-Novosibirsk_Zoo-Novosibirsk_Novosibirsky_District_Novosibirsk_Oblast_Siberian_Dist.html)",
            '[Новосибирский Музей Железнодорожной Техники](https://www.tripadvisor.ru/Attraction_Review-g298529-d2581092-Reviews-Novosibirsk_Museum_of_the_Railway_Technology-Novosibirsk_Novosibirsky_District_No.html)',
            '[Новосибирский государственный краеведческий музей](https://www.tripadvisor.ru/Attraction_Review-g298529-d307742-Reviews-Novosibirsk_State_Museum_of_Local_History_and_Nature-Novosibirsk_Novosibirsky_Dist.html)',
            '[Аквапарк Аквамир](https://www.tripadvisor.ru/Attraction_Review-g298529-d11693216-Reviews-Aquapark_Akvamir-Novosibirsk_Novosibirsky_District_Novosibirsk_Oblast_Siberian_D.html)',
            '[Центральный сибирский ботанический сад](https://www.tripadvisor.ru/Attraction_Review-g298529-d6425726-Reviews-Central_Siberian_Botanical_Garden-Novosibirsk_Novosibirsky_District_Novosibirsk_O.html)',
        ]

    elif city.lower() in {"кострома"}:
        return [
            "[Пожарная каланча](https://www.tripadvisor.ru/Attraction_Review-g298482-d2362907-Reviews-Fire_Tower-Kostroma_Kostroma_Oblast_Central_Russia.html)",
            '[Памятник Ивану Сусанину](https://www.tripadvisor.ru/Attraction_Review-g298482-d2357369-Reviews-Ivan_Susanin_Monument-Kostroma_Kostroma_Oblast_Central_Russia.html)',
            '[Беседка Островского](https://www.tripadvisor.ru/Attraction_Review-g298482-d2357382-Reviews-Ostrovsky_s_Pavilion-Kostroma_Kostroma_Oblast_Central_Russia.html)',
            '[Заброшенная военная база](https://www.tripadvisor.ru/Attraction_Review-g298482-d8672850-Reviews-Abandoned_Military_Base-Kostroma_Kostroma_Oblast_Central_Russia.html)',
            '[Особняк Борщова](https://www.tripadvisor.ru/Attraction_Review-g298482-d11827505-Reviews-House_of_Borshhov-Kostroma_Kostroma_Oblast_Central_Russia.html)',
        ]

    elif city.lower() in {"самара"}:
        return [
            "[Набережная Самары](https://www.tripadvisor.ru/Attraction_Review-g298521-d8365471-Reviews-Samara_Embankment-Samara_Samara_Oblast_Volga_District.html)",
            '[Смотровая площадка](https://www.tripadvisor.ru/Attraction_Review-g298521-d2335953-Reviews-Observation_Point-Samara_Samara_Oblast_Volga_District.html)',
            '[Ленинградская улица](https://www.tripadvisor.ru/Attraction_Review-g298521-d10308097-Reviews-Leningradskaya_Street-Samara_Samara_Oblast_Volga_District.html)',
            '[Сквер имени Александра Пушкина](https://www.tripadvisor.ru/Attraction_Review-g298521-d6439738-Reviews-Square_Aleksandra_Pushkina-Samara_Samara_Oblast_Volga_District.html)',
            '[Дом Со Слонами](https://www.tripadvisor.ru/Attraction_Review-g298521-d6650601-Reviews-House_With_Elephants-Samara_Samara_Oblast_Volga_District.html)',
        ]

    elif city.lower() in {"волгоград"}:
        return [
            "[Скульптура Родина Мать зовет](https://www.tripadvisor.ru/Attraction_Review-g298537-d5770796-Reviews-The_Motherland_Calls_Sculpture-Volgograd_Volgograd_Oblast_Southern_District.html)",
            '[Руины мельницы им Грудинина Гергардта](https://www.tripadvisor.ru/Attraction_Review-g298537-d2333786-Reviews-The_Ruins_of_the_Mill_Named_after_Grudinin_Gergardt-Volgograd_Volgograd_Oblast_So.html)',
            '[Нулевой километр Волгограда](https://www.tripadvisor.ru/Attraction_Review-g298537-d4340053-Reviews-Kilometre_Zero_of_Volgograd-Volgograd_Volgograd_Oblast_Southern_District.html)',
            '[Речной вокзал в Волгограде](https://www.tripadvisor.ru/Attraction_Review-g298537-d2333788-Reviews-River_boat_station-Volgograd_Volgograd_Oblast_Southern_District.html)',
            '[Памятник Жеглову и Шарапову](https://www.tripadvisor.ru/Attraction_Review-g298537-d10813259-Reviews-Monument_to_Zheglov_and_Sharapov-Volgograd_Volgograd_Oblast_Southern_District.html)',
        ]

    elif city.lower() in {"геленджик"}:
        return [
            "[Набережная Геленджика](https://www.tripadvisor.ru/Attraction_Review-g298514-d6781272-Reviews-Gelendzhik_Embankment-Gelendzhik_Gelendzhiksky_District_Krasnodar_Krai_Southern_D.html)",
            '[Плесецкие Водопады](https://www.tripadvisor.ru/Attraction_Review-g298514-d15009478-Reviews-Plesetskiye_Waterfalls-Gelendzhik_Gelendzhiksky_District_Krasnodar_Krai_Southern.html)',
            '[Маяк на Толстом мысу](https://www.tripadvisor.ru/Attraction_Review-g298514-d17609542-Reviews-Lighthouse_on_Cape_Tolsty-Gelendzhik_Gelendzhiksky_District_Krasnodar_Krai_South.html)',
            '[Грозовые ворота](https://www.tripadvisor.ru/Attraction_Review-g298514-d17614124-Reviews-Grozovyye_Vorota-Gelendzhik_Gelendzhiksky_District_Krasnodar_Krai_Southern_Distr.html)',
            '[Музыкальный фонтан](https://www.tripadvisor.ru/Attraction_Review-g298514-d21164427-Reviews-Musical_Fountain-Gelendzhik_Gelendzhiksky_District_Krasnodar_Krai_Southern_Distr.html)',
        ]

    elif city.lower() in {"тверь"}:
        return [
            "[Путевой дворец](https://www.tripadvisor.ru/Attraction_Review-g298487-d10547192-Reviews-Imperial_Palace-Tver_Tver_Oblast_Central_Russia.html)",
            '[Памятник Михаилу Кругу](https://www.tripadvisor.ru/Attraction_Review-g298487-d3533947-Reviews-Mikhail_Krug_Monument-Tver_Tver_Oblast_Central_Russia.html)',
            '[Набережная Афанасия Никитина](https://www.tripadvisor.ru/Attraction_Review-g298487-d8431923-Reviews-Embankment_of_Afanasiy_Nikitin-Tver_Tver_Oblast_Central_Russia.html)',
            '[Советская улица](https://www.tripadvisor.ru/Attraction_Review-g298487-d8445531-Reviews-Sovetskaya_Street-Tver_Tver_Oblast_Central_Russia.html)',
            '[Нововолжский мост](https://www.tripadvisor.ru/Attraction_Review-g298487-d13818048-Reviews-Novovolzhskiy_Bridge-Tver_Tver_Oblast_Central_Russia.html)',
        ]

    elif city.lower() in {"саратов"}:
        return [
            "[Набережная космонавтов](https://www.tripadvisor.ru/Attraction_Review-g798121-d7057948-Reviews-Spacemen_Embankment-Saratov_Saratov_Oblast_Volga_District.html)",
            '[Питомник Хаски Dogwinter](https://www.tripadvisor.ru/Attraction_Review-g798121-d15758972-Reviews-Husky_Kennel_Dogwinter-Saratov_Saratov_Oblast_Volga_District.html)',
            '[Фонтан Хрустальная слеза](https://www.tripadvisor.ru/Attraction_Review-g798121-d7227369-Reviews-Fontain_Crystal_Tear-Saratov_Saratov_Oblast_Volga_District.html  )',
            '[Городской парк "Лукоморье"](https://www.tripadvisor.ru/Attraction_Review-g798121-d4744386-Reviews-Lukomorie_Park-Saratov_Saratov_Oblast_Volga_District.html)',
            '[Эксперементаниум](https://www.tripadvisor.ru/Attraction_Review-g798121-d6490611-Reviews-Eksperimentanium_Museum_of_Entertaining_Sciences-Saratov_Saratov_Oblast_Volga_Dis.html)',
        ]

    elif city.lower() in {"красноярск"}:
        return [
            "[Красноярский Биг Бен](https://www.tripadvisor.ru/Attraction_Review-g298525-d8766908-Reviews-Krasnoyarsk_Big_Ben-Krasnoyarsk_Krasnoyarsk_Krai_Siberian_District.html)",
            '[Площадь 350 летия Красноярска](https://www.tripadvisor.ru/Attraction_Review-g298525-d307730-Reviews-350_Year_Anniversary_Square-Krasnoyarsk_Krasnoyarsk_Krai_Siberian_District.html)',
            '[Стадион Центральный](https://www.tripadvisor.ru/Attraction_Review-g298525-d2333975-Reviews-Central_Stadium-Krasnoyarsk_Krasnoyarsk_Krai_Siberian_District.html)',
            '[Памятник Александру Сергеевичу Пушкину и Наталье Гончаровой](https://www.tripadvisor.ru/Attraction_Review-g298525-d8388934-Reviews-Monument_to_Pushkin_and_Goncharova-Krasnoyarsk_Krasnoyarsk_Krai_Siberian_District.html)',
            '[Эйфелева башня](https://www.tripadvisor.ru/Attraction_Review-g298525-d13864940-Reviews-Eiffel_Tower-Krasnoyarsk_Krasnoyarsk_Krai_Siberian_District.html)',
        ]

    elif city.lower() in {"великий новгород"}:
        return [
            "[Новгородский кремль детинец](https://www.tripadvisor.ru/Attraction_Review-g298502-d304375-Reviews-Novgorod_Kremlin_Detinets-Veliky_Novgorod_Novgorod_Oblast_Northwestern_District.html)",
            '[Памятник «Тысячелетие России»](https://www.tripadvisor.ru/Attraction_Review-g298502-d304378-Reviews-Millennium_of_Russia-Veliky_Novgorod_Novgorod_Oblast_Northwestern_District.html)',
            '[Витославлицы музей народного деревянного зодчества](https://www.tripadvisor.ru/Attraction_Review-g298502-d2573237-Reviews-Vitoslavitsy_Museum_of_Folk_Wooden_Architecture-Veliky_Novgorod_Novgorod_Oblast_N.html)',
            '[Ганзейский фонтан](https://www.tripadvisor.ru/Attraction_Review-g298502-d8408304-Reviews-Ganzeyskiy_Fountain-Veliky_Novgorod_Novgorod_Oblast_Northwestern_District.html)',
            '[Кремлевский парк](https://www.tripadvisor.ru/Attraction_Review-g298502-d6502371-Reviews-Kremlin_Park-Veliky_Novgorod_Novgorod_Oblast_Northwestern_District.html)',
        ]

    elif city.lower() in {"псков"}:
        return [
            "[Псковский Кремль](https://www.tripadvisor.ru/Attraction_Review-g298503-d4945134-Reviews-Pskov_Kreml_Krom-Pskov_Pskov_Oblast_Northwestern_District.html#REVIEWS)",
            '[Псковский Государственный объединенный историко архитектурный и художественный музей заповедник](https://www.tripadvisor.ru/Attraction_Review-g298503-d2590215-Reviews-Pskov_State_Integrated_Historical_and_Architectural_and_Art_Museum_Reserve-Pskov_.html)',
            '[Гремячая башня](https://www.tripadvisor.ru/Attraction_Review-g298503-d7717483-Reviews-Gremyachaya_Tower-Pskov_Pskov_Oblast_Northwestern_District.html)',
            '[Псковский Железнодорожный Музей](https://www.tripadvisor.ru/Attraction_Review-g298503-d2590195-Reviews-Pskov_Railway_Museum-Pskov_Pskov_Oblast_Northwestern_District.html)',
            '[Музей Квартира Спегальского](https://www.tripadvisor.ru/Attraction_Review-g298503-d2590079-Reviews-Spegalsky_Apartment_Museum-Pskov_Pskov_Oblast_Northwestern_District.html)',
        ]

    elif city.lower() in {"выборг"}:
        return [
            "[Выборгский замок](https://www.tripadvisor.ru/Attraction_Review-g298511-d549147-Reviews-Vyborg_Castle-Vyborg_Vyborgsky_District_Leningrad_Oblast_Northwestern_District.html)",
            '[Старый город](https://www.tripadvisor.ru/Attraction_Review-g298511-d14975905-Reviews-Old_City-Vyborg_Vyborgsky_District_Leningrad_Oblast_Northwestern_District.html)',
            '[Старый Выборгский рынок](https://www.tripadvisor.ru/Attraction_Review-g298511-d10092532-Reviews-Old_Vyborg_Market-Vyborg_Vyborgsky_District_Leningrad_Oblast_Northwestern_Distri.html)',
            '[Часовая башня](https://www.tripadvisor.ru/Attraction_Review-g298511-d3578063-Reviews-Clock_Tower-Vyborg_Vyborgsky_District_Leningrad_Oblast_Northwestern_District.html)',
            '[Памятник выборгскому трамваю](https://www.tripadvisor.ru/Attraction_Review-g298511-d17227790-Reviews-Monument_to_the_Vyborg_Tram-Vyborg_Vyborgsky_District_Leningrad_Oblast_Northwest.html)',
        ]

    elif city.lower() in {"переславль-залесский", "переславль залесский"}:
        return [
            "[Национальный парк Плещеево озеро](https://www.tripadvisor.ru/Attraction_Review-g1572253-d6942908-Reviews-National_Park_Lake_Pleshheyevo-Pereslavl_Zalessky_Yaroslavl_Oblast_Central_Russi.html)",
            '[Музей крестьянского дизайна "Конь в пальто"](https://www.tripadvisor.ru/Attraction_Review-g1572253-d12475638-Reviews-Museum_of_Peasant_Design_Kon_V_Palto-Pereslavl_Zalessky_Yaroslavl_Oblast_Centra.html#REVIEWS)',
            '[Памятник Ленину](https://www.tripadvisor.ru/Attraction_Review-g1572253-d16926305-Reviews-Lenin_Monument-Pereslavl_Zalessky_Yaroslavl_Oblast_Central_Russia.html)',
            '[Музей утюга](https://www.tripadvisor.ru/Attraction_Review-g1572253-d2357304-Reviews-Museum_of_Flat_Irons-Pereslavl_Zalessky_Yaroslavl_Oblast_Central_Russia.html)',
            '[Фабрика Павлова](https://www.tripadvisor.ru/Attraction_Review-g1572253-d16875241-Reviews-Pavlov_Factory-Pereslavl_Zalessky_Yaroslavl_Oblast_Central_Russia.html)',
        ]

    elif city.lower() in {"рязань"}:
        return [
            "[Памятник Сергею Есенину](https://www.tripadvisor.ru/Attraction_Review-g298485-d2360403-Reviews-Sergey_Yesenin_Monument-Ryazan_Ryazan_Oblast_Central_Russia.html)",
            '[Памятник Надежде Чумаковой](https://www.tripadvisor.ru/Attraction_Review-g298485-d7162066-Reviews-Monument_to_Nadezhda_Chumakova-Ryazan_Ryazan_Oblast_Central_Russia.html)',
            '[Набережная реки Трубеж](https://www.tripadvisor.ru/Attraction_Review-g298485-d17430454-Reviews-Embankment_of_the_River_Trubezh-Ryazan_Ryazan_Oblast_Central_Russia.html)',
            '[Триумфальные ворота](https://www.tripadvisor.ru/Attraction_Review-g298485-d16771101-Reviews-Triumfalnyye_Vorota-Ryazan_Ryazan_Oblast_Central_Russia.html)',
            '[Кинотеатр Родина](https://www.tripadvisor.ru/Attraction_Review-g298485-d16738155-Reviews-Cinema_Rodina-Ryazan_Ryazan_Oblast_Central_Russia.html)',
        ]

    elif city.lower() in {"коломна"}:
        return [
            "[Соборная площадь](https://www.tripadvisor.ru/Attraction_Review-g798120-d10324016-Reviews-Sobornaya_Square-Kolomna_Moscow_Oblast_Central_Russia.html)",
            '[Памятник Владимиру Ильичу Ленину](https://www.tripadvisor.ru/Attraction_Review-g798120-d9463210-Reviews-V_I_Lenin_Monument-Kolomna_Moscow_Oblast_Central_Russia.html)',
            '[Памятник Паровоза Л0012](https://www.tripadvisor.ru/Attraction_Review-g798120-d2363059-Reviews-Monument_to_L_0012_Type_Steam_Locomotive-Kolomna_Moscow_Oblast_Central_Russia.html)',
            '[Житная площадь](https://www.tripadvisor.ru/Attraction_Review-g798120-d12611158-Reviews-Zhitnaya_Square-Kolomna_Moscow_Oblast_Central_Russia.html)',
            '[Памятник Дмитрию Донскому](https://www.tripadvisor.ru/Attraction_Review-g798120-d8471357-Reviews-Statue_of_Dmitriy_Donskoi-Kolomna_Moscow_Oblast_Central_Russia.html)',
        ]

    elif city.lower() in {"сергиев посад", "сергиев-посад"}:
        return [
            '[Музейный комплекс "Конный двор"](https://www.tripadvisor.ru/Attraction_Review-g445047-d2357264-Reviews-The_Museum_Complex_Konny_Dvor-Sergiyev_Posad_Sergiyevo_Posadsky_District_Moscow_O.html)',
            '[Культурно просветительский центр Дубрава](https://www.tripadvisor.ru/Attraction_Review-g445047-d12695036-Reviews-Dubrava_Cultural_and_Educational_Center-Sergiyev_Posad_Sergiyevo_Posadsky_Distri.html)',
            '[Сергиевская кухмистерская](https://www.tripadvisor.ru/Attraction_Review-g445047-d14210680-Reviews-Sergiyevskaya_Kukhmisterskaya-Sergiyev_Posad_Sergiyevo_Posadsky_District_Moscow_.html)',
            '[Музей советского детства](https://www.tripadvisor.ru/Attraction_Review-g445047-d15329270-Reviews-Museum_of_Soviet_Childhood-Sergiyev_Posad_Sergiyevo_Posadsky_District_Moscow_Obl.html)',
            '[Музей Впечатлений](https://www.tripadvisor.ru/Attraction_Review-g445047-d14062202-Reviews-Muzey_Vpechatlenii-Sergiyev_Posad_Sergiyevo_Posadsky_District_Moscow_Oblast_Cent.html)',
        ]

    elif city.lower() in {"уфа"}:
        return [
            "[Фонтан Семь девушек](https://www.tripadvisor.ru/Attraction_Review-g298518-d8422798-Reviews-Fountain_Seven_Girls-Ufa_Republic_of_Bashkortostan_Volga_District.html)",
            '[Монумент Дружбы](https://www.tripadvisor.ru/Attraction_Review-g298518-d2330993-Reviews-Monument_Druzhby-Ufa_Republic_of_Bashkortostan_Volga_District.html)',
            '[Нулевой километр](https://www.tripadvisor.ru/Attraction_Review-g298518-d11777822-Reviews-Zero_Kilometer-Ufa_Republic_of_Bashkortostan_Volga_District.html)',
            '[Памятник Ленину](https://www.tripadvisor.ru/Attraction_Review-g298518-d7653913-Reviews-Lenin_Statue-Ufa_Republic_of_Bashkortostan_Volga_District.html)',
            '[Еврейский национальный культурный центр](https://www.tripadvisor.ru/Attraction_Review-g298518-d5504575-Reviews-Jewish_National_Cultural_Center-Ufa_Republic_of_Bashkortostan_Volga_District.html)',
        ]

    elif city.lower() in {"кисловодск"}:
        return [
            "[Нарзанная Галерея](https://www.tripadvisor.ru/Attraction_Review-g675740-d6538860-Reviews-Narzannaya_Gallery-Kislovodsk_Stavropol_Krai_North_Caucasian_District.html)",
            '[Поющий фонтан](https://www.tripadvisor.ru/Attraction_Review-g675740-d11777342-Reviews-The_Singing_Fountain-Kislovodsk_Stavropol_Krai_North_Caucasian_District.html)',
            '[Каскадная лестница](https://www.tripadvisor.ru/Attraction_Review-g675740-d7139719-Reviews-Cascade_Stairs-Kislovodsk_Stavropol_Krai_North_Caucasian_District.html)',
            '[Храм воздуха](https://www.tripadvisor.ru/Attraction_Review-g675740-d8780986-Reviews-Air_Temple-Kislovodsk_Stavropol_Krai_North_Caucasian_District.html)',
            '[Замок любви и коварства](https://www.tripadvisor.ru/Attraction_Review-g675740-d6697354-Reviews-Castle_of_Love_and_Deceit-Kislovodsk_Stavropol_Krai_North_Caucasian_District.html)',
        ]

    elif city.lower() in {"ростов-на-дону", "ростов на дону", "ростов"}:
        return [
            "[Ростов Арена](https://www.tripadvisor.ru/Attraction_Review-g298535-d14001686-Reviews-Rostov_Arena-Rostov_on_Don_Rostov_Oblast_Southern_District.html)",
            '[Ворошиловский мост](https://www.tripadvisor.ru/Attraction_Review-g298535-d9723389-Reviews-Voroshilovskiy_Bridge-Rostov_on_Don_Rostov_Oblast_Southern_District.html)',
            '[Парамоновские склады](https://www.tripadvisor.ru/Attraction_Review-g298535-d6580257-Reviews-Paramonovskie_Sklady-Rostov_on_Don_Rostov_Oblast_Southern_District.html)',
            '[Сантехник с котом](https://www.tripadvisor.ru/Attraction_Review-g298535-d12507003-Reviews-Plumber_with_Cat-Rostov_on_Don_Rostov_Oblast_Southern_District.html)',
            '[Театральная площадь](https://www.tripadvisor.ru/Attraction_Review-g298535-d9723391-Reviews-Teatralnaya_Square-Rostov_on_Don_Rostov_Oblast_Southern_District.html)',
        ]

    elif city.lower() in {"смоленск"}:
        return [
            "[Смоленская Крепость](https://www.tripadvisor.ru/Attraction_Review-g672719-d2604018-Reviews-Smolensk_Fortress-Smolensk_Smolensky_District_Smolensk_Oblast_Central_Russia.html)",
            '[Стадион "Спартак"](https://www.tripadvisor.ru/Attraction_Review-g672719-d8044636-Reviews-Spartak-Smolensk_Smolensky_District_Smolensk_Oblast_Central_Russia.html)',
            '[Набережная реки Днепр](https://www.tripadvisor.ru/Attraction_Review-g672719-d10308839-Reviews-Embankment_of_the_Dnepr_River-Smolensk_Smolensky_District_Smolensk_Oblast_Centra.html)',
            '[Памятник Твардовскому и Теркину](https://www.tripadvisor.ru/Attraction_Review-g672719-d8178863-Reviews-Monument_to_Aleksandr_Tvardovskiy_and_Vasiliy_Tyorkin-Smolensk_Smolensky_District.html)',
            '[Скульптура Олень](https://www.tripadvisor.ru/Attraction_Review-g672719-d8178874-Reviews-Sculpture_Deer-Smolensk_Smolensky_District_Smolensk_Oblast_Central_Russia.html)',
        ]

    elif city.lower() in {"челябинск", "челяба", "город-героин россии"}:
        return [
            "[Челябинский Арбат](https://www.tripadvisor.ru/Attraction_Review-g298539-d2337523-Reviews-Pedestrian_Street_Kirovka-Chelyabinsk_Chelyabinsk_Oblast_Urals_District.html)",
            '[Нулевой километр](https://www.tripadvisor.ru/Attraction_Review-g298539-d8600654-Reviews-Kilometer_Zero-Chelyabinsk_Chelyabinsk_Oblast_Urals_District.html)',
            '[Ледовая арена "Трактор"](https://www.tripadvisor.ru/Attraction_Review-g298539-d6440177-Reviews-Ice_Arena_Traktor-Chelyabinsk_Chelyabinsk_Oblast_Urals_District.html)',
            '[Памятник профессиональному нищему](https://www.tripadvisor.ru/Attraction_Review-g298539-d2337526-Reviews-Monument_of_Beggar_Man-Chelyabinsk_Chelyabinsk_Oblast_Urals_District.html)',
            '[Скульптура Левша, подковавший блоху](https://www.tripadvisor.ru/Attraction_Review-g298539-d8600626-Reviews-Sculpture_Levsha_Podkovavshiy_Blokhu-Chelyabinsk_Chelyabinsk_Oblast_Urals_Distric.html)',
        ]

    elif city.lower() in {"пятигорск"}:
        return [
            "[Озеро Провал](https://www.tripadvisor.ru/Attraction_Review-g298534-d4402061-Reviews-Proval_Lake-Pyatigorsk_Stavropol_Krai_North_Caucasian_District.html)",
            '[Канатная дорога](https://www.tripadvisor.ru/Attraction_Review-g298534-d8152595-Reviews-Cable_Car-Pyatigorsk_Stavropol_Krai_North_Caucasian_District.html)',
            '[Парк Цветник](https://www.tripadvisor.ru/Attraction_Review-g298534-d3948556-Reviews-Tsvetnik_Park-Pyatigorsk_Stavropol_Krai_North_Caucasian_District.html)',
            '[Скульптура Орла](https://www.tripadvisor.ru/Attraction_Review-g298534-d3335266-Reviews-The_Eagle_Mounument-Pyatigorsk_Stavropol_Krai_North_Caucasian_District.html)',
            '[Памятник на месте дуэли Михаила Юрьевича Лермонтова](https://www.tripadvisor.ru/Attraction_Review-g298534-d7132900-Reviews-Monument_to_Lermontov_at_the_Place_of_Duel-Pyatigorsk_Stavropol_Krai_North_Caucas.html)',
        ]

    elif city.lower() in {"иваново"}:
        return [
            "[Дом корабль](https://www.tripadvisor.ru/Attraction_Review-g790136-d2360578-Reviews-House_Ship-Ivanovo_Ivanovsky_District_Ivanovo_Oblast_Central_Russia.html)",
            '[Щудровская палатка](https://www.tripadvisor.ru/Attraction_Review-g790136-d2360589-Reviews-Shchudrovskaya_Palatka-Ivanovo_Ivanovsky_District_Ivanovo_Oblast_Central_Russia.html)',
            '[Памятник Бездомным животным "Верность"](https://www.tripadvisor.ru/Attraction_Review-g790136-d15619242-Reviews-Monument_to_the_Homeless_Animals_Vernost-Ivanovo_Ivanovsky_District_Ivanovo_Obla.html)',
            '[Дом пуля](https://www.tripadvisor.ru/Attraction_Review-g790136-d8353116-Reviews-Bullet_House-Ivanovo_Ivanovsky_District_Ivanovo_Oblast_Central_Russia.html)',
            '[Дом птица](https://www.tripadvisor.ru/Attraction_Review-g790136-d8353118-Reviews-Bird_House-Ivanovo_Ivanovsky_District_Ivanovo_Oblast_Central_Russia.html)',
        ]

    elif city.lower() in {"калуга"}:
        return [
            "[Памятник Циолковскому (человек с велосипедом)](https://www.tripadvisor.ru/Attraction_Review-g798118-d9734257-Reviews-Monument_to_Tsiolkovskiy_a_Man_with_a_Bicycle-Kaluga_Kaluga_Oblast_Central_Russia.html)",
            '[Каменный мост](https://www.tripadvisor.ru/Attraction_Review-g798118-d8762895-Reviews-Stone_Bridge-Kaluga_Kaluga_Oblast_Central_Russia.html)',
            '[Памятник Циолковскому](https://www.tripadvisor.ru/Attraction_Review-g798118-d8714367-Reviews-Monument_to_Tsiolkovskiy-Kaluga_Kaluga_Oblast_Central_Russia.html)',
            '[Памятник 600 летия Калуги](https://www.tripadvisor.ru/Attraction_Review-g798118-d8784989-Reviews-Monument_to_the_600th_Anniversary_of_Kaluga-Kaluga_Kaluga_Oblast_Central_Russia.html)',
            '[Набережная Калуги](https://www.tripadvisor.ru/Attraction_Review-g798118-d10606711-Reviews-Kaluga_Embankment-Kaluga_Kaluga_Oblast_Central_Russia.html)',
        ]

    elif city.lower() in {"пермь"}:
        return [
            "[Арт-объект Счастье не за горами](https://www.tripadvisor.ru/Attraction_Review-g298516-d7155886-Reviews-Art_Object_Schastye_ne_za_Gorami-Perm_Perm_Krai_Volga_District.html)",
            '[Пермский академический Театр](https://www.tripadvisor.ru/Attraction_Review-g298516-d2332073-Reviews-Perm_Academic_Theatre-Perm_Perm_Krai_Volga_District.html)',
            '[Набережная реки Камы](https://www.tripadvisor.ru/Attraction_Review-g298516-d15188915-Reviews-River_Kamy_Embankment-Perm_Perm_Krai_Volga_District.html)',
            '[Пермская Ротонда](https://www.tripadvisor.ru/Attraction_Review-g298516-d2332055-Reviews-The_Rotunda-Perm_Perm_Krai_Volga_District.html)',
            '[Фонтан Театральный](https://www.tripadvisor.ru/Attraction_Review-g298516-d8453968-Reviews-Fountain_Teatralny-Perm_Perm_Krai_Volga_District.html)',
        ]

    elif city.lower() in {"тюмень"}:
        return [
            "[Мост Влюбленных](https://www.tripadvisor.ru/Attraction_Review-g662362-d3533929-Reviews-Bridge_of_Lovers_Pedestrian-Tyumen_Tyumen_Oblast_Urals_District.html)",
            '[Музыкальные часы с колоколами](https://www.tripadvisor.ru/Attraction_Review-g662362-d7137962-Reviews-Musical_Watch_With_Bells-Tyumen_Tyumen_Oblast_Urals_District.html)',
            '[Городской сад](https://www.tripadvisor.ru/Attraction_Review-g662362-d8390801-Reviews-City_Garden-Tyumen_Tyumen_Oblast_Urals_District.html)',
            '[Памятник Паровоз ФД21 3031](https://www.tripadvisor.ru/Attraction_Review-g662362-d10121593-Reviews-Monument_Locomotive_FD21_3031-Tyumen_Tyumen_Oblast_Urals_District.html)',
            '[Площадь Единства и Согласия](https://www.tripadvisor.ru/Attraction_Review-g662362-d8445362-Reviews-Square_of_Unity_and_Reconciliation-Tyumen_Tyumen_Oblast_Urals_District.html)',
        ]

    elif city.lower() in {"омск"}:
        return [
            "[Пожарная Каланча](https://www.tripadvisor.ru/Attraction_Review-g298530-d8178793-Reviews-Fire_Observation_Tower-Omsk_Omsk_Oblast_Siberian_District.html)",
            '[Тарские ворота](https://www.tripadvisor.ru/Attraction_Review-g298530-d2325738-Reviews-Tara_Gates-Omsk_Omsk_Oblast_Siberian_District.html)',
            '[Площадь Бухгольца](https://www.tripadvisor.ru/Attraction_Review-g298530-d2325747-Reviews-Buchholz_Square-Omsk_Omsk_Oblast_Siberian_District.html)',
            '[Памятник весы бытия](https://www.tripadvisor.ru/Attraction_Review-g298530-d8680499-Reviews-Monument_The_Scales_of_Existence-Omsk_Omsk_Oblast_Siberian_District.html)',
            '[Памятник Дон Кихоту](https://www.tripadvisor.ru/Attraction_Review-g298530-d7622825-Reviews-Don_Quixote_Monument-Omsk_Omsk_Oblast_Siberian_District.html)',
        ]

    elif city.lower() in {"йошкар-ола", "йошкар ола"}:
        return [
            "[Набережная Брюгге](https://www.tripadvisor.ru/Attraction_Review-g777986-d5508387-Reviews-Brugge_Embankment-Yoshkar_Ola_Mari_El_Republic_Volga_District.html)",
            '[Часы 12 апостолов](https://www.tripadvisor.ru/Attraction_Review-g777986-d10224661-Reviews-Clock_12_Apostles-Yoshkar_Ola_Mari_El_Republic_Volga_District.html)',
            '[Бульвар Чавайна](https://www.tripadvisor.ru/Attraction_Review-g777986-d8261429-Reviews-Chavaina_Boulevard-Yoshkar_Ola_Mari_El_Republic_Volga_District.html)',
            '[Благовещенская башня](https://www.tripadvisor.ru/Attraction_Review-g777986-d10307079-Reviews-Blagoveshhenskaya_Tower-Yoshkar_Ola_Mari_El_Republic_Volga_District.html)',
            '[Солнечные часы](https://www.tripadvisor.ru/Attraction_Review-g777986-d13080871-Reviews-Sun_Clock-Yoshkar_Ola_Mari_El_Republic_Volga_District.html)',
        ]

    elif city.lower() in {"новороссийск"}:
        return [
            "[Арт парк](https://www.tripadvisor.ru/Attraction_Review-g2641383-d10470164-Reviews-Art_Park-Abrau_Durso_Novorossiysk_Krasnodar_Krai_Southern_District.html)",
            '[Дольменный комплекс](https://www.tripadvisor.ru/Attraction_Review-g660729-d7155641-Reviews-Dolmen_Complex-Novorossiysk_Krasnodar_Krai_Southern_District.html)',
            '[Памятник основателям города Новороссийск](https://www.tripadvisor.ru/Attraction_Review-g660729-d8587389-Reviews-Monument_to_the_Founders_of_Novorossiysk-Novorossiysk_Krasnodar_Krai_Southern_Dis.html)',
            '[Центральный Стадион](https://www.tripadvisor.ru/Attraction_Review-g660729-d8044385-Reviews-Central_Stadium-Novorossiysk_Krasnodar_Krai_Southern_District.html)',
            '[Руины дворца культуры цементников](https://www.tripadvisor.ru/Attraction_Review-g660729-d6474903-Reviews-Ruins_of_the_Palace_of_Culture-Novorossiysk_Krasnodar_Krai_Southern_District.html)',
        ]

    elif city.lower() in {"владивосток"}:
        return [
            "[Маяк Эгершельд](https://www.tripadvisor.ru/Attraction_Review-g298496-d2331334-Reviews-Lighthouse_Egersheld-Vladivostok_Primorsky_Krai_Far_Eastern_District.html)",
            '[Russky Bridge](https://www.tripadvisor.ru/Attraction_Review-g298496-d3438580-Reviews-Russky_Bridge-Vladivostok_Primorsky_Krai_Far_Eastern_District.html)',
            '[Сопка Орлиное гнездо](https://www.tripadvisor.ru/Attraction_Review-g298496-d1790949-Reviews-Eagle_s_Nest_Mount-Vladivostok_Primorsky_Krai_Far_Eastern_District.html)',
            '[Золотой Mост](https://www.tripadvisor.ru/Attraction_Review-g298496-d5503600-Reviews-Golden_Bridge-Vladivostok_Primorsky_Krai_Far_Eastern_District.html)',
            '[Набережная Цесаревича](https://www.tripadvisor.ru/Attraction_Review-g298496-d4555877-Reviews-Tsesarevich_Embankment-Vladivostok_Primorsky_Krai_Far_Eastern_District.html)',
        ]

    elif city.lower() in {"углич"}:
        return [
            "[Пожарная каланча](https://www.tripadvisor.ru/Attraction_Review-g951352-d10681883-Reviews-Fire_Observation_Tower-Uglich_Yaroslavl_Oblast_Central_Russia.html)",
            '[Музей ремёсел и творчества "Дом с наличниками"](https://www.tripadvisor.ru/Attraction_Review-g951352-d12590736-Reviews-Crafts_Museum_and_Art_House_with_Frames-Uglich_Yaroslavl_Oblast_Central_Russia.html)',
            '[Никольский Соборный Мост](https://www.tripadvisor.ru/Attraction_Review-g951352-d12624438-Reviews-Nikolskiy_Cathedral_Bridge-Uglich_Yaroslavl_Oblast_Central_Russia.html)',
            '[Памятник Ленину](https://www.tripadvisor.ru/Attraction_Review-g951352-d8619701-Reviews-Statue_of_Lenin-Uglich_Yaroslavl_Oblast_Central_Russia.html)',
            '[Памятник царевичу Дмитрию](https://www.tripadvisor.ru/Attraction_Review-g951352-d21338635-Reviews-Tsarevitch_Dmitriy_Monument-Uglich_Yaroslavl_Oblast_Central_Russia.html)',
        ]

    elif city.lower() in {"петрозаводск"}:
        return [
            "[Набережная Онежского озера](https://www.tripadvisor.ru/Attraction_Review-g298506-d8780951-Reviews-Embankment_of_Lake_Onega-Petrozavodsk_Republic_of_Karelia_Northwestern_District.html)",
            '[Памятник Петру Первому](https://www.tripadvisor.ru/Attraction_Review-g298506-d549135-Reviews-Monument_to_Peter_the_Great-Petrozavodsk_Republic_of_Karelia_Northwestern_District.html)',
            '[Квартал исторической застройки Петрозаводска](https://www.tripadvisor.ru/Attraction_Review-g298506-d6942260-Reviews-Block_of_Historic_Buildings_Petrozavodsk-Petrozavodsk_Republic_of_Karelia_Northwe.html)',
            '[Дом куклы Татьяны Калининой](https://www.tripadvisor.ru/Attraction_Review-g298506-d6502746-Reviews-Tatyana_Kalinina_s_Doll_House-Petrozavodsk_Republic_of_Karelia_Northwestern_Distr.html)',
            '[Маленькая страна](https://www.tripadvisor.ru/Attraction_Review-g298506-d6491270-Reviews-Small_Country_Lyubov_Malinovskaya_s_Creative_Center-Petrozavodsk_Republic_of_Kare.html)',
        ]

    elif city.lower() in {"вологда"}:
        return [
            "[Резной Палисад](https://www.tripadvisor.ru/Attraction_Review-g445048-d8514604-Reviews-Center_of_National_Art_Crafts_Reznoi_Palisad-Vologda_Vologda_Oblast_Northwestern_.html)",
            '[Памятник букве "О"](https://www.tripadvisor.ru/Attraction_Review-g445048-d3812825-Reviews-Letter_O_Monument-Vologda_Vologda_Oblast_Northwestern_District.html)',
            '[Скамейка Посидим поокаем](https://www.tripadvisor.ru/Attraction_Review-g445048-d9841640-Reviews-Bench_Posidim_Pookayem-Vologda_Vologda_Oblast_Northwestern_District.html)',
            '[Городище, место основания  Вологды](https://www.tripadvisor.ru/Attraction_Review-g445048-d8281706-Reviews-Ancient_Town_Place_of_Vologda_Foundation-Vologda_Vologda_Oblast_Northwestern_Dist.html)',
            '[Памятник Рубцову](https://www.tripadvisor.ru/Attraction_Review-g445048-d9884040-Reviews-Monument_to_Rubtsov-Vologda_Vologda_Oblast_Northwestern_District.html)',
        ]

    elif city.lower() in {"муром"}:
        return [
            "[Вантовый мост через Оку](https://www.tripadvisor.ru/Attraction_Review-g777989-d2360256-Reviews-Cable_braced_bridge-Murom_Vladimir_Oblast_Central_Russia.html)",
            '[Памятник Илье Муромцу](https://www.tripadvisor.ru/Attraction_Review-g777989-d2360266-Reviews-Statue_of_Ilya_Muromets-Murom_Vladimir_Oblast_Central_Russia.html)',
            '[Былинный камень](https://www.tripadvisor.ru/Attraction_Review-g777989-d2360264-Reviews-Murom_Bylina_Stone-Murom_Vladimir_Oblast_Central_Russia.html)',
            '[Муромский калач](https://www.tripadvisor.ru/Attraction_Review-g777989-d10423020-Reviews-Murom_Kalatch-Murom_Vladimir_Oblast_Central_Russia.html)',
            '[Муромская Водонапорная башня](https://www.tripadvisor.ru/Attraction_Review-g777989-d8599887-Reviews-Murom_Water_Tower-Murom_Vladimir_Oblast_Central_Russia.html)',
        ]

    elif city.lower() in {"астрахань"}:
        return [
            "[Астраханский Кремль](https://www.tripadvisor.ru/Attraction_Review-g298513-d3333678-Reviews-Astrakhan_Kremlin-Astrakhan_Astrakhan_Oblast_Southern_District.html)",
            '[Городская набережная](https://www.tripadvisor.ru/Attraction_Review-g298513-d7084682-Reviews-City_Embankment-Astrakhan_Astrakhan_Oblast_Southern_District.html)',
            '[Музыкальный фонтан](https://www.tripadvisor.ru/Attraction_Review-g298513-d5533816-Reviews-Music_Fountain-Astrakhan_Astrakhan_Oblast_Southern_District.html)',
            '[Скульптура "Дама с собачкой"](https://www.tripadvisor.ru/Attraction_Review-g298513-d7695777-Reviews-Sculpture_Lady_with_the_Dog-Astrakhan_Astrakhan_Oblast_Southern_District.html)',
            '[Сарай Бату](https://www.tripadvisor.ru/Attraction_Review-g298513-d5617654-Reviews-Saraj_Batu-Astrakhan_Astrakhan_Oblast_Southern_District.html)',
        ]

    elif city.lower() in {"ростов великий"}:
        return [
            "[Пожарная каланча](https://www.tripadvisor.ru/Attraction_Review-g811396-d12614037-Reviews-Fire_Observation_Tower-Rostov_Yaroslavl_Oblast_Central_Russia.html)",
            '[Пристань "Подозёрка"](https://www.tripadvisor.ru/Attraction_Review-g811396-d15056647-Reviews-Pristan_Podozyorka-Rostov_Yaroslavl_Oblast_Central_Russia.html)',
            '[Олень, символ Ростова Великого](https://www.tripadvisor.ru/Attraction_Review-g811396-d23319566-Reviews-Deer_symbol_of_Rostov_the_Great-Rostov_Yaroslavl_Oblast_Central_Russia.html)',
            '[Гостиный двор](https://www.tripadvisor.ru/Attraction_Review-g811396-d23017102-Reviews-Gostiny_Dvor-Rostov_Yaroslavl_Oblast_Central_Russia.html)',
            '[Закладной камень памятника ростовскому князю Василько](https://www.tripadvisor.ru/Attraction_Review-g811396-d23011138-Reviews-Foundation_Stone_of_the_Monument_to_the_Rostov_Prince_Vasilko-Rostov_Yaroslavl_O.html)',
        ]

    elif city.lower() in {"плёс", "плес"}:
        return [
            "[Набережная Плеса](https://www.tripadvisor.ru/Attraction_Review-g2619278-d7148949-Reviews-Ples_Embankment-Ples_Ivanovo_Oblast_Central_Russia.html)",
            '[Соборная гора](https://www.tripadvisor.ru/Attraction_Review-g2619278-d11487995-Reviews-Sobornaya_Mountain-Ples_Ivanovo_Oblast_Central_Russia.html)',
            '[Памятник кошке](https://www.tripadvisor.ru/Attraction_Review-g2619278-d8613835-Reviews-Statue_of_the_Cat-Ples_Ivanovo_Oblast_Central_Russia.html)',
            '[Скульптура Дачница](https://www.tripadvisor.ru/Attraction_Review-g2619278-d8603114-Reviews-Sculpture_Dachnitsa-Ples_Ivanovo_Oblast_Central_Russia.html)',
            '[Торговая площадь](https://www.tripadvisor.ru/Attraction_Review-g2619278-d15070797-Reviews-Torgovaya_Square-Ples_Ivanovo_Oblast_Central_Russia.html)',
        ]

    elif city.lower() in {"звенигород"}:
        return [
            "[Конное подворье Дютьково](https://www.tripadvisor.ru/Attraction_Review-g2347166-d7004538-Reviews-Horse_Farm_Dutkovo-Zvenigorod_Moscow_Oblast_Central_Russia.html)",
            '[Купчий Двор](https://www.tripadvisor.ru/Attraction_Review-g2347166-d11778932-Reviews-Kupchiy_Dvor-Zvenigorod_Moscow_Oblast_Central_Russia.html)',
            '[Памятник Чехову](https://www.tripadvisor.ru/Attraction_Review-g2347166-d11980217-Reviews-Monument_to_Chekhov-Zvenigorod_Moscow_Oblast_Central_Russia.html)',
            '[Усадьба Введенское](https://www.tripadvisor.ru/Attraction_Review-g2347166-d8139699-Reviews-Vvedenskoye_Estate-Zvenigorod_Moscow_Oblast_Central_Russia.html)',
            '[Дом Чехова](https://www.tripadvisor.ru/Attraction_Review-g2347166-d12539404-Reviews-House_of_Chekhov-Zvenigorod_Moscow_Oblast_Central_Russia.html)',
        ]

    elif city.lower() in {"липецк"}:
        return [
            "[Памятник в честь 300 летия Липецка](https://www.tripadvisor.ru/Attraction_Review-g798123-d10383337-Reviews-Monument_in_Honor_of_the_300th_Anniversary_of_Lipetsk-Lipetsk_Lipetsk_Oblast_Cen.html)",
            '[Липецкий областной центр культуры и народного творчества](https://www.tripadvisor.ru/Attraction_Review-g798123-d12864694-Reviews-Lipetsk_Regional_Center_of_Culture-Lipetsk_Lipetsk_Oblast_Central_Russia.html)',
            '[Каскад фонтанов](https://www.tripadvisor.ru/Attraction_Review-g798123-d12847279-Reviews-Cascade_of_Fountains-Lipetsk_Lipetsk_Oblast_Central_Russia.html)',
            '[Липецкое Городище](https://www.tripadvisor.ru/Attraction_Review-g798123-d15780225-Reviews-Lipetskoye_Gorodishhe-Lipetsk_Lipetsk_Oblast_Central_Russia.html)',
            '[Центральный стадион Металлург](https://www.tripadvisor.ru/Attraction_Review-g798123-d8044135-Reviews-Metallurg-Lipetsk_Lipetsk_Oblast_Central_Russia.html)',
        ]

    else:
        return None
        # return ['Такого города нет в базе данных, пожалуйста, проверьте правильность написания']
