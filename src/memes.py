import os, random

def meme():
    for root, dirs, files in os.walk("./src/memes"):  
        with open(root + '/' + random.choice(files), 'rb') as meme_file:
            return meme_file.read()